import os
import argparse
from nn.operations import str2bool
from tensorflow import config

try:
    from .directories import homedir
except ImportError:
    homedir = os.curdir


def get_args():
    desc = "TF2 implementation of a Self-Supervised Class-Attention Cross-Covariance Visual Transformer"
    parser = argparse.ArgumentParser(description=desc)

    parser.add_argument("--model", nargs="?", type=str, default="xcit_tst",
                        help="The model name",
                        choices=["xcit_tst", "cait_xxs24", "cait_xs36", "cait_xs24",
                                 "cait_s24", "cait_m24", "cait_s48", "cait_s36",
                                 "xcit_s12", "xcit_s24", "xcit_m24",
                                 "efficient_net_b0", "efficient_net_b1", "efficient_net_b2",
                                 "efficient_net_b3", "efficient_net_b4", "efficient_net_b5",
                                 "efficient_net_b6", "efficient_net_b7", ])

    parser.add_argument('--wavelength', help='Wavelength used in the experiment in nm', type=float, default=45.)

    parser.add_argument('--angle', help='Maximum scattering angle of the detector in degrees', type=float, default=30)

    parser.add_argument('--delta', help="Real part of the refractive index (X-Ray Nomenclature: n'= 1 - delta)",
                        type=float, default=-0.27368)

    parser.add_argument('--beta', help='Imaginary part of the refractive index', type=float, default=0.702404)

    parser.add_argument('--exp_weight', help="The weighting factor for constructing the exponential weights.  "
                                             "This value is by how much the 10th order should weigh "
                                             "-> in percent points, so 1 = 100%", type=float, default=0.1)

    parser.add_argument('--mean_radius', help='Mean radius of the target particles in units of the wavelength',
                        type=float, default=800.)

    parser.add_argument('--degree',
                        help='The max degree (l value) up to which spherical harmonics should be calculated',
                        type=int, default=8)

    parser.add_argument("--train_steps_per_epochs", nargs="?", type=int, default=int(10000),
                        help="Since we sample from an infinite generator we set an arbitrary epoch length")

    parser.add_argument("--eval_steps_per_epochs", nargs="?", type=int, default=int(100),
                        help="Since we sample from an infinite generator we set an arbitrary epoch length")

    parser.add_argument("--epochs", nargs="?", type=int, default=int(301),
                        help="The number of epochs to run")

    parser.add_argument("--save_every_n_epoch", nargs="?", type=int, default=10,
                        help="Save model weights every 'n' epochs")

    parser.add_argument("--test_every_n_epoch", nargs="?", type=int, default=5,
                        help="Evaluate model every 'n' epochs")

    parser.add_argument("--image_size", nargs="?", type=int, default=224,
                        help="Image size to which the input will be resized")

    parser.add_argument("--image_channels", nargs="?", type=int, default=1,
                        help="Image channel of the input data")

    parser.add_argument("--log_steps", nargs="?", type=int, default=500,
                        help="Write out logs every 'n' steps")

    parser.add_argument("--batch_size", nargs="?", type=int, default=int(16),
                        help="The size of batch per gpu.")

    parser.add_argument("--clr_temperature", nargs="?", type=float, default=float(0.5),
                        help="The temperature parameter for the contrastive task during pre-training")

    parser.add_argument("--optimizer", nargs="?", type=str, default="adamw",
                        help="The used optimizer. Choices are either 'sgd', 'adam', 'adamw', 'rms'",
                        choices=["sgd", "adam", "adamw", "rms"])

    parser.add_argument("--loss_function", nargs="?", type=str, default="mae",
                        help="The used loss function. Choices are 'contrastive_loss', 'huber', 'mae', "
                             "'msle', 'mape', 'mse', 'cross_entropy'",
                        choices=['contrastive_loss', 'huber', 'mae', 'msle', 'mape', 'mse', 'cross_entropy'])

    parser.add_argument("--lr", nargs="?", type=float, default=.0,
                        help="Learning rate. Leave zero for batch-size dependent estimation of the lr'")

    parser.add_argument("--l2", nargs="?", type=float, default=0.05,
                        help="L2 regularization term. Sometimes called 'weight decay'")

    parser.add_argument("--deep_root", nargs="?", type=str2bool, default="true",
                        help="Use a deep root instead of the standard ViT patchify stem",
                        choices=[True, False])

    parser.add_argument("--sam", nargs="?", type=str2bool, default="true",
                        help="Use sharpness-aware minimization",
                        choices=[True, False])

    parser.add_argument("--sam_rho", type=float, default=0.1,
                        help="Rho for the sharpness-aware minimization")

    parser.add_argument("--use_fourier_embedding", nargs="?", type=str2bool, default="true",
                        help="Use a Fourier Embedding as in the original Vaswani Paper",
                        choices=[True, False])

    parser.add_argument("--minimize_power_spectrum", nargs="?", type=str2bool, default="true",
                        help="Minimize the power spectrum instead of the full harmonic spectrum",
                        choices=[True, False])

    parser.add_argument("--patch_size", nargs="?", type=int, default=int(8),
                        help="The input patch size, can be 16, 8, or 4",
                        choices=[int(16), int(8), int(4)])

    parser.add_argument("--base_model_dir", nargs="?", type=str,
                        default="",
                        help="This argument is only used in the fine-tune and eval routine,"
                             "and provides a path to a base model to use. This"
                             "base model has to be pre-trained.")

    parser.add_argument("--base_model_epoch", nargs="?", type=str, default="",
                        help="This argument is only used in the fine-tune and eval routine,"
                             "The epoch to load. Leave this empty for the latest model.")

    parser.add_argument("--fp16", nargs="?", type=str2bool, default="true",
                        help="Use mixed precision or not. Set to True if your NVidia GPU "
                             "has compute capability > 7.0",
                        choices=[True, False])

    parser.add_argument("--augs_on_gpu", nargs="?", type=str2bool, default="true",
                        help="Should we doe the image augmentations on the GPU instead of CPU",
                        choices=[True, False])

    parser.add_argument("--scatman_gpu", help='Idx of the GPU scatman should use',
                        type=int, default=0)

    parser.add_argument("--gpus", nargs="?", type=str, default="all",
                        help="Number of GPUs to be used. Can be index-based '0,1,2' or "
                             "'all' for all available")

    parser.add_argument("--work_dir", nargs="?", type=str, default=homedir,
                        help="The work directory. "
                             "Missing directories will be created")

    return check_args(parser.parse_args())


def check_args(args):
    """
    Checking arguments
    """
    try:
        assert args.epochs >= 1
    except AssertionError:
        args.logger.error("\t[!] Number of epochs must be larger than or equal to one")

    if sum([x.find("GPU:") for x in args.gpus]) < 0:  # Entries like "GPU:0" can be kept
        if "all" in args.gpus:
            gpus = config.list_physical_devices("GPU")
            args.gpus = [x.name[x.name.find(":") + 1:] for x in gpus]
        else:
            gpus = list(args.gpus)
            for ii in gpus:  # Get rid of every non integer in the string
                try:
                    int(ii)
                except ValueError:
                    gpus = list(filter(ii.__ne__, gpus))
            gpus.sort()
            args.gpus = ["GPU:{}".format(x) for x in gpus]

    if len(args.gpus) < 1:
        args.logger.error("\t[!] Please provide a valid argument for the gpu selector")

    return args

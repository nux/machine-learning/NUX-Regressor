import resource

low, high = resource.getrlimit(resource.RLIMIT_NOFILE)
resource.setrlimit(resource.RLIMIT_NOFILE, (high, high))
import nn
import numpy as np
import tensorflow as tf

tfk = tf.keras
tfkl = tfk.layers


# noinspection PyProtectedMember
def prep_ds(ds_single, args, ty="train"):
    """
    Prepare the dataset
    :param ds_single: Tensorflow dataset
    :param args: The args dataframe
    :param ty: Either 'pretrain_eval', 'pretrain_train', 'eval', or 'train'
    :return: a Tensorflow dataset
    """

    # Cache, shuffle, and batch everything
    if args.augs_on_gpu:
        img_shape = (args.image_size, args.image_size, 1)
    else:
        img_shape = (args.image_size, args.image_size, 3)

    options = tf.data.Options()
    options.experimental_distribute.auto_shard_policy = tf.data.experimental.AutoShardPolicy.DATA
    options.deterministic = False
    ds = ds_single.with_options(options)

    # Cut out the center
    ds = ds.map(nn.center_cutout, num_parallel_calls=tf.data.AUTOTUNE)

    # Pre Normalize the logged image between 0 and 1
    ds = ds.map(lambda img, lbl: nn.normalize_img(img, lbl,
                                                  use_image_normalization=False),
                num_parallel_calls=tf.data.AUTOTUNE)
    # ds = ds.map(lambda img, lbl: nn.normalize_img(img, lbl,
    #                                               use_image_normalization=False,
    #                                               use_log=True),
    #             num_parallel_calls=tf.data.AUTOTUNE)

    if args.augs_on_gpu:
        kwargs = dict(img_dims=img_shape, mode="gpu")
    else:
        if "train" in ty:
            kwargs = dict(img_dims=img_shape, mode=ty)
        else:
            kwargs = dict(img_dims=img_shape, mode=ty,
                          color_distort=False, flip=False)

    ds = ds.map(lambda x, y: nn.base_augment(x, y, **kwargs),
                num_parallel_calls=tf.data.AUTOTUNE)
    # Add detector hole
    ds = ds.map(lambda x, y: nn.det_hole(x, y, args),
                num_parallel_calls=tf.data.AUTOTUNE)

    # TODO: Pretrain distortion needs to be rewritten for to work on the GPU itself
    if args.pretrain and not args.augs_on_gpu:
        ds = ds.map(lambda x, y: (tf.stop_gradient(
            tf.py_function(nn.distort_images, [x], tf.float32)
        ), y), num_parallel_calls=tf.data.AUTOTUNE)

    # TODO: Check if this is still correct. Not sure if channels are expanded prior to that.
    # Back to Grayscale
    if not args.augs_on_gpu:
        ds = ds.map(lambda x, y: nn.expand_channels(x, y, to_gray=True, expand_to_last_dim=False),
                    num_parallel_calls=tf.data.AUTOTUNE)

    if not args.augs_on_gpu:
        if "efficient" in args.model:
            # Normalize to work with EfficientNet
            # For EfficientNet, input preprocessing is included as part of the model (as a Rescaling layer),
            # EfficientNet models expect their inputs to be float tensors of pixels with values in the [0-255] range.
            norm_kwargs = {"new_max": 255, "use_image_normalization": False}
        else:
            norm_kwargs = {"use_image_normalization": True}
    else:
        norm_kwargs = {"use_image_normalization": False}

    ds = ds.map(lambda img, lbl: nn.normalize_img(img, lbl, **norm_kwargs),
                num_parallel_calls=tf.data.AUTOTUNE)
    ds = ds.prefetch(tf.data.AUTOTUNE)
    return ds


def get_data(args):
    """
    Unified data pipeline
    :param args: The args dataframe
    :return:
    """
    scatman_uncropped_image_size = np.round(args.image_size / np.cos(np.pi / 4)).astype(int)
    out_shape = (args.batch_size,
                 int(3 + args.degree) if args.minimize_power_spectrum else int(2 + np.square(args.degree + 1)))
    data = tf.data.Dataset.from_generator(nn.ScatmanGenerator,
                                          output_signature=(
                                              tf.TensorSpec(
                                                  shape=(args.batch_size, scatman_uncropped_image_size,
                                                         scatman_uncropped_image_size, 1)),
                                              tf.TensorSpec(
                                                  shape=out_shape)
                                          ),
                                          args=(args.batch_size,  # "bs"
                                                args.wavelength,  # "wavelength"
                                                args.angle,  # "angle"
                                                args.image_size,  # "resolution"
                                                args.delta,  # "delta"
                                                args.beta,  # "beta"
                                                args.mean_radius,  # "mean_radius"
                                                args.degree,  # "degree"
                                                args.scatman_gpu,  # "gpus"
                                                args.exp_weight,  # exp weight factor
                                                False,  # "use_cpu"
                                                args.minimize_power_spectrum  # power_spectrum
                                                )
                                          )
    if args.pretrain:
        eval_ty = "pretrain_eval"
        train_ty = "pretrain_train"
    else:
        eval_ty = "eval"
        train_ty = "train"

    data = prep_ds(ds_single=data, args=args,
                   ty=train_ty)

    return data

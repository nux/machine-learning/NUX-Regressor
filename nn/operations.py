import os
import nn
import re
import sys
import math
import random
import datetime
import numpy as np
import tensorflow as tf
import tensorflow_probability as tfp

from abc import ABCMeta, ABC
from collections import namedtuple

tfkm = tf.keras.metrics


class StochasticDepth(tf.keras.layers.Layer):
    """Stochastic Depth layer.

    Implements Stochastic Depth as described in
    [Deep Networks with Stochastic Depth](https://arxiv.org/abs/1603.09382), to randomly drop residual branches
    in residual architectures.

    Args:
        drop_rate: float, the drop_rate (1 - survival probability) of the residual branch being kept.

    Call Args:
        inputs:  List of `[shortcut, residual]` where `shortcut`, and `residual` are tensors of equal shape.

    Output shape:
        Equal to the shape of inputs `shortcut`, and `residual`
    """

    def __init__(self, drop_rate: float = 0.5, **kwargs):
        super().__init__(**kwargs)

        self.survival_probability = 1. - drop_rate

    def call(self, x, training=None):
        if not isinstance(x, list) or len(x) != 2:
            raise ValueError("input must be a list of length 2.")

        shortcut, residual = x

        # Random bernoulli variable indicating whether the branch should be kept or not or not
        b_l = tf.keras.backend.random_bernoulli([], p=self.survival_probability)

        def _call_train():
            return shortcut + tf.cast(b_l, self.compute_dtype) * residual

        def _call_test():
            return shortcut + self.survival_probability * residual

        return tf.keras.backend.in_train_phase(
            _call_train, _call_test, training=training
        )

    @staticmethod
    def compute_output_shape(input_shape):
        return input_shape[0]

    def get_config(self):
        base_config = super().get_config()

        config = {"survival_probability": self.survival_probability}

        return {**base_config, **config}


class LinearCosineSchedule(tf.keras.optimizers.schedules.LearningRateSchedule, metaclass=ABCMeta):
    def __init__(self, initial_learning_rate, warmup_steps, total_steps):
        """
        The learning scheduler increases linearly from 0 to "initial_learning_rate" for
        about "warmup_steps" steps. After the warmup, the learning rate decreases by a
        cosine schedule towards zero until the training ends.

        See:
        1. He, T. et al. Bag of Tricks for Image Classification with Convolutional Neural Networks.
        Proc. IEEE Comput. Soc. Conf. Comput. Vis. Pattern Recognit. 2019-June, 558–567 (2018).

        :param initial_learning_rate: The learning rate after the warmup phase
        :param warmup_steps: The number of warmup steps
        :param total_steps: The total number of steps for training
        """
        super(LinearCosineSchedule, self).__init__()

        self.initial_learning_rate = tf.cast(initial_learning_rate, tf.float32)
        self.total_steps = tf.cast(total_steps, tf.float32)
        self.warmup_steps = tf.cast(tf.math.maximum(1, warmup_steps), tf.float32)

    def __call__(self, step):
        arg1 = self.cosine(tf.cast(step, tf.float32) - self.warmup_steps)
        m = tf.math.divide(self.initial_learning_rate, self.warmup_steps)
        arg2 = tf.math.multiply(m, tf.cast(step, tf.float32))

        return tf.math.minimum(arg1, arg2)

    def cosine(self, step):
        cos_arg = tf.math.divide(step, self.total_steps)
        cos_ = tf.math.cos(tf.math.multiply(math.pi, cos_arg))
        mul_lr = tf.math.multiply(.5, 1 + cos_)
        return tf.math.multiply(mul_lr, self.initial_learning_rate)


def str2bool(x):
    return x.lower() in "true"


def check_folder(directory, return_path=False):
    if not os.path.exists(directory):
        os.makedirs(directory)
    if return_path:
        return directory


def set_path_env(args, create_folders=True):
    if args.pretrain:
        train_mode_str = "pretrain"
    else:
        train_mode_str = "scratch"
    # else:
    #     raise LookupError("finetune or pretrain should be defined in the args dictionary")

    suffix = "{}_{}_{}".format(args.batch_size,
                               args.model,
                               train_mode_str)

    # Setting up the paths
    path_tuple = namedtuple("path_tuple", "work_dir weight_dir log_dir base_dir")
    if create_folders:
        a = check_folder(args.work_dir, True)
        if hasattr(args, 'paths'):
            base_dir = args.paths.base_dir
        else:
            current_time = datetime.datetime.now().strftime("%d_%m_%Y-%H_%M_%S")
            base_dir = os.path.join(a, current_time + "_" + suffix)
        b = check_folder(os.path.join(base_dir, "weights"), True)
        c = check_folder(os.path.join(base_dir, "logs"), True)
    else:
        a = args.work_dir
        current_time = datetime.datetime.now().strftime("%d_%m_%Y-%H_%M_%S")
        base_dir = os.path.join(a, current_time + "_" + suffix)
        b = os.path.join(base_dir, "weights")
        c = os.path.join(base_dir, "logs")

    paths = path_tuple(work_dir=a, weight_dir=b, log_dir=c, base_dir=base_dir)
    return paths


# noinspection PyProtectedMember
def get_model(args):
    # If we use a helium dataset, then we don"t have a dedicated test and train set.
    # We, therefore, have to manually set how many examples are in the test and train dataset
    output_dim = int(3 + args.degree) if args.minimize_power_spectrum else int(2 + np.square(args.degree + 1))
    train_steps_per_epoch = args.train_steps_per_epochs
    eval_steps_per_epoch = args.eval_steps_per_epochs
    total_train_steps = math.floor(train_steps_per_epoch * args.epochs)

    # Image shape
    img_shape = [args.image_size, args.image_size, args.image_channels]

    if len(img_shape) == 2:
        batch_shape = [None] + img_shape + [1]
    elif len(img_shape) == 4:
        batch_shape = [None] + img_shape[1:]
    else:
        batch_shape = [None] + img_shape

    transformer_kwargs = dict(input_shape=batch_shape,
                              output_dim=output_dim,
                              pretrain=args.pretrain,
                              deep_root=args.deep_root,
                              patch_size=args.patch_size,
                              use_fourier_embedding=args.use_fourier_embedding,
                              l2=0 if args.optimizer.lower() == "adamw" else args.l2)
    if args.model.lower() == "xcit_tst":
        model = nn.xcit_tst(**transformer_kwargs)
    elif args.model.lower() == "cait_xxs24":
        model = nn.cait_xxs24(**transformer_kwargs)
    elif args.model.lower() == "cait_xs36":
        model = nn.cait_xs36(**transformer_kwargs)
    elif args.model.lower() == "cait_xs24":
        model = nn.cait_xs24(**transformer_kwargs)
    elif args.model.lower() == "cait_s24":
        model = nn.cait_s24(**transformer_kwargs)
    elif args.model.lower() == "cait_m24":
        model = nn.cait_m24(**transformer_kwargs)
    elif args.model.lower() == "cait_s48":
        model = nn.cait_s48(**transformer_kwargs)
    elif args.model.lower() == "cait_s36":
        model = nn.cait_s36(**transformer_kwargs)
    elif args.model.lower() == "xcit_s24":
        model = nn.xcit_s24(**transformer_kwargs)
    elif args.model.lower() == "xcit_m24":
        model = nn.xcit_m24(**transformer_kwargs)
    elif args.model.lower() == "xcit_s12":
        model = nn.xcit_s12(**transformer_kwargs)
    elif args.model.lower() == "efficient_net_b0":
        model = nn.efficientnetb0(output_dim=output_dim,
                                  input_shape=img_shape)
    elif args.model.lower() == "efficient_net_b1":
        model = nn.efficientnetb1(output_dim=output_dim,
                                  input_shape=img_shape)
    elif args.model.lower() == "efficient_net_b2":
        model = nn.efficientnetb2(output_dim=output_dim,
                                  input_shape=img_shape)
    elif args.model.lower() == "efficient_net_b3":
        model = nn.efficientnetb3(output_dim=output_dim,
                                  input_shape=img_shape)
    elif args.model.lower() == "efficient_net_b4":
        model = nn.efficientnetb4(output_dim=output_dim,
                                  input_shape=img_shape)
    elif args.model.lower() == "efficient_net_b5":
        model = nn.efficientnetb5(output_dim=output_dim,
                                  input_shape=img_shape)
    elif args.model.lower() == "efficient_net_b6":
        model = nn.efficientnetb6(output_dim=output_dim,
                                  input_shape=img_shape)
    elif args.model.lower() == "efficient_net_b7":
        model = nn.efficientnetb7(output_dim=output_dim,
                                  input_shape=img_shape)
    else:
        raise LookupError("Please use a valid model. Your choice was: {} which is "
                          "not in the list of allowed models.".format(args.model.lower()))

    if "efficient" in args.model.lower() and args.optimizer.lower() != "adamw":
        # Add inplace weight decay to EfficientNets
        add_weight_decay(model, args.l2)

    nt_str = "output_dim total_steps batch_shape steps_per_epoch eval_steps_per_epoch"
    train_info_tuple = namedtuple("train_info_tuple",
                                  nt_str)

    train_info = train_info_tuple(
        output_dim=output_dim,
        total_steps=total_train_steps,
        batch_shape=batch_shape,
        steps_per_epoch=train_steps_per_epoch,
        eval_steps_per_epoch=eval_steps_per_epoch
    )

    return model, train_info


def get_metrics(args, pretrain=False):
    if pretrain:
        metric_list = ["total_loss", "regularization_loss", "contrast_loss",
                       "weighted_contrast_loss", "reconstruction_loss",
                       "weighted_reconstruction_loss", "rotational_loss",
                       "weighted_rotational_loss", "contrast_accuracy",
                       "contrast_entropy", "rotational_weight",
                       "contrastive_weight", "reconstructive_weight",
                       "rotational_accuracy"]
        metric_str = ""
        for a in ["pretrain_train", "pretrain_eval"]:
            base = "__{} ".format(a)
            metric_str += base.join(metric_list) + base
        metric_str = metric_str.replace("regularization_loss__pretrain_eval", "")
        metric_str = metric_str.replace("rotational_weight__pretrain_eval contrastive_"
                                        "weight__pretrain_eval reconstructive_weight__pretrain_eval", "")

        metrics_tuple = namedtuple("metrics_tuple", metric_str)

        # Train Losses
        total_loss_train = tfkm.Mean("total_loss_train")
        regularization_loss_train = tfkm.Mean("regularization_loss_train")
        contrast_loss_train = tfkm.Mean("contrast_loss_train")
        weighted_contrast_loss_train = tfkm.Mean("weighted_contrast_loss_train")
        reconstruction_loss_train = tfkm.Mean("reconstruction_loss_train")
        weighted_reconstruction_loss_train = tfkm.Mean("weighted_reconstruction_loss_train")
        rotational_loss_train = tfkm.Mean("rotational_loss_train")
        weighted_rotational_loss_train = tfkm.Mean("weighted_rotational_loss_train")

        # Train Metrics
        contrast_accuracy_train = tfkm.Mean("contrast_accuracy_train")
        contrast_entropy_train = tfkm.Mean("contrast_entropy_train")
        rotational_weight = tfkm.Mean("rotational_weight")
        contrastive_weight = tfkm.Mean("contrastive_weight")
        reconstructive_weight = tfkm.Mean("reconstructive_weight")
        rotational_accuracy = tfkm.Accuracy(name="rotational_accuracy")

        # Eval Losses
        total_loss_eval = tfkm.Mean("total_loss_eval")
        contrast_loss_eval = tfkm.Mean("contrast_loss_eval")
        weighted_contrast_loss_eval = tfkm.Mean("weighted_contrast_loss_eval")
        reconstruction_loss_eval = tfkm.Mean("reconstruction_loss_eval")
        weighted_reconstruction_loss_eval = tfkm.Mean("weighted_reconstruction_loss_eval")
        rotational_loss_eval = tfkm.Mean("rotational_loss_eval")
        weighted_rotational_loss_eval = tfkm.Mean("weighted_rotational_loss_eval")

        # Eval Metrics
        contrast_accuracy_eval = tfkm.Mean("contrast_accuracy_eval")
        contrast_entropy_eval = tfkm.Mean("contrast_entropy_eval")
        rotational_accuracy_eval = tfkm.Accuracy(name="rotational_accuracy_eval")

        eval_metrics = [total_loss_train, regularization_loss_train, contrast_loss_train,
                        weighted_contrast_loss_train, reconstruction_loss_train,
                        weighted_reconstruction_loss_train, rotational_loss_train,
                        weighted_rotational_loss_train, contrast_accuracy_train,
                        contrast_entropy_train, rotational_weight,
                        contrastive_weight, reconstructive_weight, rotational_accuracy,
                        total_loss_eval, contrast_loss_eval, weighted_contrast_loss_eval,
                        reconstruction_loss_eval, weighted_reconstruction_loss_eval,
                        rotational_loss_eval, weighted_rotational_loss_eval,
                        contrast_accuracy_eval, contrast_entropy_eval, rotational_accuracy_eval]
    else:
        metric_list = ["total_loss", "task_loss", "regularization_loss",
                       "channel_wise_loss", "channel_wise_logits", "channel_wise_labels", "mape"]
        if "full_eval" in args.__dir__():
            metric_list += ["total_loss_log", "channel_wise_loss_log", "channel_wise_logits_log",
                            "channel_wise_labels_log", "total_loss_sigmoid", "channel_wise_loss_sigmoid",
                            "channel_wise_logits_sigmoid", "channel_wise_labels_sigmoid", "total_loss_norm",
                            "channel_wise_loss_norm", "channel_wise_logits_norm", "channel_wise_labels_norm",
                            "total_loss_sqrt", "channel_wise_loss_sqrt", "channel_wise_logits_sqrt",
                            "channel_wise_labels_sqrt"
                            ]
        metric_str = ""
        for a in ["train", "eval"]:
            base = "__{} ".format(a)
            metric_str += base.join(metric_list) + base
        metric_str = metric_str.replace("task_loss__eval", "")
        metric_str = metric_str.replace("regularization_loss__eval", "")
        if "full_eval" in args.__dir__():
            metric_str = metric_str.replace("total_loss_log__train", "")
            metric_str = metric_str.replace("channel_wise_loss_log__train", "")
            metric_str = metric_str.replace("channel_wise_logits_log__train", "")
            metric_str = metric_str.replace("channel_wise_labels_log__train", "")
            metric_str = metric_str.replace("total_loss_sigmoid__train", "")
            metric_str = metric_str.replace("channel_wise_loss_sigmoid__train", "")
            metric_str = metric_str.replace("channel_wise_logits_sigmoid__train", "")
            metric_str = metric_str.replace("channel_wise_labels_sigmoid__train", "")
            metric_str = metric_str.replace("total_loss_norm__train", "")
            metric_str = metric_str.replace("channel_wise_loss_norm__train", "")
            metric_str = metric_str.replace("channel_wise_logits_norm__train", "")
            metric_str = metric_str.replace("channel_wise_labels_norm__train", "")
            metric_str = metric_str.replace("total_loss_sqrt__train", "")
            metric_str = metric_str.replace("channel_wise_loss_sqrt__train", "")
            metric_str = metric_str.replace("channel_wise_logits_sqrt__train", "")
            metric_str = metric_str.replace("channel_wise_labels_sqrt__train", "")

        metrics_tuple = namedtuple("metrics_tuple", metric_str)

        # Train losses
        shape_mean_tensors = [
            int(3 + args.degree) if args.minimize_power_spectrum else int(2 + np.square(args.degree + 1))]
        total_loss_train = tfkm.Mean(name="total_loss_train")
        task_loss_train = tfkm.Mean(name="task_loss_train")
        regularization_loss_train = tfkm.Mean(name="regularization_loss_train")
        channel_wise_loss_train = tfkm.MeanTensor(name="channel_wise_loss_train", dtype=tf.float64,
                                                  shape=shape_mean_tensors)
        channel_wise_logits_train = tfkm.MeanTensor(name="channel_wise_logits_train", dtype=tf.float64,
                                                    shape=shape_mean_tensors)
        channel_wise_labels_train = tfkm.MeanTensor(name="channel_wise_labels_train", dtype=tf.float64,
                                                    shape=shape_mean_tensors)
        mape_train = tfkm.MeanAbsolutePercentageError(name="mape_train")

        # Eval Losses
        total_loss_eval = tfkm.Mean(name="total_loss_eval")
        channel_wise_loss_eval = tfkm.MeanTensor(name="channel_wise_loss_eval", dtype=tf.float64,
                                                 shape=shape_mean_tensors)
        channel_wise_logits_eval = tfkm.MeanTensor(name="channel_wise_logits_eval", dtype=tf.float64,
                                                   shape=shape_mean_tensors)
        channel_wise_labels_eval = tfkm.MeanTensor(name="channel_wise_labels_eval", dtype=tf.float64,
                                                   shape=shape_mean_tensors)
        mape_eval = tfkm.MeanAbsolutePercentageError(name="mape_eval")

        eval_metrics = [total_loss_train, task_loss_train, regularization_loss_train,
                        channel_wise_loss_train, channel_wise_logits_train,
                        channel_wise_labels_train, mape_train, total_loss_eval, channel_wise_loss_eval,
                        channel_wise_logits_eval, channel_wise_labels_eval, mape_eval
                        ]

        if "full_eval" in args.__dir__():
            total_loss_eval_log = tfkm.Mean(name="total_loss_eval_log")
            channel_wise_loss_eval_log = tfkm.MeanTensor(name="channel_wise_loss_eval_log", dtype=tf.float64,
                                                         shape=shape_mean_tensors)
            channel_wise_logits_eval_log = tfkm.MeanTensor(name="channel_wise_logits_eval_log", dtype=tf.float64,
                                                           shape=shape_mean_tensors)
            channel_wise_labels_eval_log = tfkm.MeanTensor(name="channel_wise_labels_eval_log", dtype=tf.float64,
                                                           shape=shape_mean_tensors)

            total_loss_eval_sigmoid = tfkm.Mean(name="total_loss_eval_sigmoid")
            channel_wise_loss_eval_sigmoid = tfkm.MeanTensor(name="channel_wise_loss_eval_sigmoid", dtype=tf.float64,
                                                             shape=shape_mean_tensors)
            channel_wise_logits_eval_sigmoid = tfkm.MeanTensor(name="channel_wise_logits_eval_sigmoid",
                                                               dtype=tf.float64,
                                                               shape=shape_mean_tensors)
            channel_wise_labels_eval_sigmoid = tfkm.MeanTensor(name="channel_wise_labels_eval_sigmoid",
                                                               dtype=tf.float64,
                                                               shape=shape_mean_tensors)

            total_loss_eval_norm = tfkm.Mean(name="total_loss_eval_norm")
            channel_wise_loss_eval_norm = tfkm.MeanTensor(name="channel_wise_loss_eval_norm", dtype=tf.float64,
                                                          shape=shape_mean_tensors)
            channel_wise_logits_eval_norm = tfkm.MeanTensor(name="channel_wise_logits_eval_norm", dtype=tf.float64,
                                                            shape=shape_mean_tensors)
            channel_wise_labels_eval_norm = tfkm.MeanTensor(name="channel_wise_labels_eval_norm", dtype=tf.float64,
                                                            shape=shape_mean_tensors)

            total_loss_eval_sqrt = tfkm.Mean(name="total_loss_eval_sqrt")
            channel_wise_loss_eval_sqrt = tfkm.MeanTensor(name="channel_wise_loss_eval_sqrt", dtype=tf.float64,
                                                          shape=shape_mean_tensors)
            channel_wise_logits_eval_sqrt = tfkm.MeanTensor(name="channel_wise_logits_eval_sqrt", dtype=tf.float64,
                                                            shape=shape_mean_tensors)
            channel_wise_labels_eval_sqrt = tfkm.MeanTensor(name="channel_wise_labels_eval_sqrt", dtype=tf.float64,
                                                            shape=shape_mean_tensors)

            eval_metrics += [total_loss_eval_log, channel_wise_loss_eval_log, channel_wise_logits_eval_log,
                             channel_wise_labels_eval_log, total_loss_eval_sigmoid, channel_wise_loss_eval_sigmoid,
                             channel_wise_logits_eval_sigmoid, channel_wise_labels_eval_sigmoid, total_loss_eval_norm,
                             channel_wise_loss_eval_norm, channel_wise_logits_eval_norm, channel_wise_labels_eval_norm,
                             total_loss_eval_sqrt, channel_wise_loss_eval_sqrt, channel_wise_logits_eval_sqrt,
                             channel_wise_labels_eval_sqrt]

    keys = list(filter(None, metric_str.split(" ")))
    metrics = metrics_tuple(**dict(zip(keys, eval_metrics)))
    return metrics


def get_loss(fun):
    if fun == "contrastive_loss":
        return contrastive_loss
    elif fun == "huber":
        return huber
    elif fun == "mae":
        return mae
    elif fun == "msle":
        return msle
    elif fun == "mape":
        return mape
    elif fun == "mse":
        return mse
    elif fun == "cross_entropy":
        return cross_entropy
    else:
        print("Select a valid loss function. Choices are 'contrastive_loss', 'huber', 'mae', "
              "'msle', 'mape', 'mse', 'cross_entropy'")
        exit()


def contrastive_loss(hidden1, hidden2, args,
                     epsilon=1e9):
    """Compute loss for model.
    Args:
      hidden1: hidden vector 1 (`Tensor`) of shape (bsz, dim).
      hidden2: hidden vector 2 (`Tensor`) of shape (bsz, dim).
      args: The args dict
      use_cosine: If the cosine variant of this loss should be used
      epsilon: Large Number for numerical stability
    Returns:
      A loss scalar.
      The logits for contrastive prediction task.
      The labels for contrastive prediction task.
    """
    # Get (normalized) hidden1 and hidden2.
    bs = tf.shape(hidden1)[0]
    z_i = tf.math.l2_normalize(hidden1, 1)
    z_j = tf.math.l2_normalize(hidden2, 1)

    logits_ab = tf.matmul(z_i, tf.identity(z_j), transpose_b=True) / args.clr_temperature
    labels = tf.one_hot(tf.range(bs), bs * 2)

    z_i_large = tf.identity(z_i)
    z_j_large = tf.identity(z_j)

    masks = tf.one_hot(tf.range(bs), bs)

    logits_aa = tf.matmul(z_i, z_i_large, transpose_b=True) / args.clr_temperature
    logits_aa = logits_aa - masks * epsilon

    logits_bb = tf.matmul(z_j, z_j_large, transpose_b=True) / args.clr_temperature
    logits_bb = logits_bb - masks * epsilon
    logits_ba = tf.matmul(z_j, z_i_large, transpose_b=True) / args.clr_temperature

    loss_a = tf.nn.softmax_cross_entropy_with_logits(
        labels, tf.concat([logits_ab, logits_aa], 1))
    loss_b = tf.nn.softmax_cross_entropy_with_logits(
        labels, tf.concat([logits_ba, logits_bb], 1))

    losses = loss_a + loss_b  # [bs, 1]

    # Accuracy
    con_acc = tf.equal(tf.math.argmax(logits_ab, axis=-1),
                       tf.math.argmax(labels, axis=-1))

    # Return also the logits, for entropy calculation
    return losses, con_acc, logits_ab


def huber(labels, preds, args):
    """Compute huber / SmoothedL1 loss over local batch."""
    losses = tf.keras.losses.Huber(
        delta=1.0,
        reduction=tf.keras.losses.Reduction.NONE)(y_true=labels,
                                                  y_pred=preds)  # [2 * bs if pretrain else bs, 1]
    bs_norm = (2. * args.batch_size) if args.pretrain else args.batch_size
    losses = tf.reduce_sum(losses) * (1. / bs_norm)
    return losses


def mae(labels, preds, args):
    """Compute mean absolute error over local batch."""
    losses = tf.keras.losses.MeanAbsoluteError(
        reduction=tf.keras.losses.Reduction.NONE)(y_true=labels,
                                                  y_pred=preds)  # [2 * bs if pretrain else bs, height, width, channel]

    # Compute loss that is scaled by sample_weight and by global batch size.
    bs_norm = (2. * args.batch_size) if args.pretrain else args.batch_size
    losses = tf.reduce_sum(losses) * (1. / bs_norm)
    return losses


def msle(labels, preds, args):
    """
    Compute relative log accuracy over local batch.
    The optimal model will converge to the geometric mean.
    see Tofallis, C. A better measure of relative prediction accuracy for
    model selection and model estimation. J. Oper. Res. Soc. 66, 1352–1362 (2015)
    """
    losses = tf.keras.losses.MeanSquaredLogarithmicError(
        reduction=tf.keras.losses.Reduction.NONE)(y_true=labels,
                                                  y_pred=preds)  # [2 * bs if pretrain else bs, height, width, channel]

    # Compute loss that is scaled by sample_weight and by global batch size.
    bs_norm = (2. * args.batch_size) if args.pretrain else args.batch_size
    losses = tf.reduce_sum(losses) * (1. / bs_norm)
    return losses


def mape(labels, preds, args):
    """Compute mean absolute percentage error over local batch."""
    losses = tf.keras.losses.MeanAbsolutePercentageError(
        reduction=tf.keras.losses.Reduction.NONE)(y_true=labels,
                                                  y_pred=preds)  # [2 * bs if pretrain else bs, height, width, channel]

    # Compute loss that is scaled by sample_weight and by global batch size.
    bs_norm = (2. * args.batch_size) if args.pretrain else args.batch_size
    losses = tf.reduce_sum(losses) * (1. / bs_norm)
    return losses


def mse(labels, preds, args):
    """Compute mean squared error over local batch."""
    losses = tf.keras.losses.MeanSquaredError(
        reduction=tf.keras.losses.Reduction.NONE)(y_true=labels,
                                                  y_pred=preds)  # [2 * bs if pretrain else bs, height, width, channel]

    # Compute loss that is scaled by sample_weight and by global batch size.
    bs_norm = (2. * args.batch_size) if args.pretrain else args.batch_size
    losses = tf.reduce_sum(losses) * (1. / bs_norm)
    return losses


def cross_entropy(labels, logits, args):
    """Compute cross entropy loss over local batch."""
    losses = tf.keras.losses.CategoricalCrossentropy(
        reduction=tf.keras.losses.Reduction.NONE,
        from_logits=True)(y_true=labels,
                          y_pred=logits)  # [2 * bs if pretrain else bs, 1]
    bs_norm = (2. * args.batch_size) if args.pretrain else args.batch_size
    losses = tf.reduce_sum(losses) * (1. / bs_norm)
    return losses


# noinspection PyProtectedMember
class AdamWeightDecay(tf.keras.optimizers.Adam):
    """
    Adam enables L2 weight decay and clip_by_global_norm on gradients. Just adding the square of the weights to the
    loss function is *not* the correct way of using L2 regularization/weight decay with Adam, since that will interact
    with the m and v parameters in strange ways as shown in `Decoupled Weight Decay Regularization
    <https://arxiv.org/abs/1711.05101>`__.

    Instead we want ot decay the weights in a manner that doesn't interact with the m/v parameters. This is equivalent
    to adding the square of the weights to the loss with plain (non-momentum) SGD.

    Args:
        learning_rate (:obj:`Union[float, tf.keras.optimizers.schedules.LearningRateSchedule]`, `optional`,
            defaults to 1e-3): The learning rate to use or a schedule.
        beta_1 (:obj:`float`, `optional`, defaults to 0.9):
            The beta1 parameter in Adam, which is the exponential decay rate for the 1st momentum estimates.
        beta_2 (:obj:`float`, `optional`, defaults to 0.999):
            The beta2 parameter in Adam, which is the exponential decay rate for the 2nd momentum estimates.
        epsilon (:obj:`float`, `optional`, defaults to 1e-7):
            The epsilon parameter in Adam, which is a small constant for numerical stability.
        amsgrad (:obj:`bool`, `optional`, default to `False`):
            Whether to apply AMSGrad variant of this algorithm or not, see `On the Convergence of Adam and Beyond
            <https://arxiv.org/abs/1904.09237>`__.
        weight_decay_rate (:obj:`float`, `optional`, defaults to 0):
            The weight decay to apply.
        include_in_weight_decay (:obj:`List[str]`, `optional`):
            List of the parameter names (or re patterns) to apply weight decay to. If none is passed, weight decay is
            applied to all parameters by default (unless they are in :obj:`exclude_from_weight_decay`).
        exclude_from_weight_decay (:obj:`List[str]`, `optional`):
            List of the parameter names (or re patterns) to exclude from applying weight decay to. If a
            :obj:`include_in_weight_decay` is passed, the names in it will supersede this list.
        name (:obj:`str`, `optional`, defaults to 'AdamWeightDecay'):
            Optional name for the operations created when applying gradients.
        kwargs:
            Keyword arguments. Allowed to be {``clipnorm``, ``clipvalue``, ``lr``, ``decay``}. ``clipnorm`` is clip
            gradients by norm; ``clipvalue`` is clip gradients by value, ``decay`` is included for backward
            compatibility to allow time inverse decay of learning rate. ``lr`` is included for backward compatibility,
            recommended to use ``learning_rate`` instead.
    """

    def __init__(
            self,
            learning_rate=0.001,
            beta_1: float = 0.9,
            beta_2: float = 0.999,
            epsilon: float = 1e-7,
            amsgrad: bool = False,
            weight_decay_rate: float = 0.0,
            include_in_weight_decay=None,
            exclude_from_weight_decay=None,
            name: str = "AdamWeightDecay",
            **kwargs
    ):
        super().__init__(learning_rate, beta_1, beta_2, epsilon, amsgrad, name, **kwargs)
        self.weight_decay_rate = weight_decay_rate
        self._include_in_weight_decay = include_in_weight_decay
        self._exclude_from_weight_decay = exclude_from_weight_decay

    @classmethod
    def from_config(cls, config, **kwargs):
        """Creates an optimizer from its config with WarmUp custom object.
        """
        return super(AdamWeightDecay, cls).from_config(config)

    def _prepare_local(self, var_device, var_dtype, apply_state):
        super(AdamWeightDecay, self)._prepare_local(var_device, var_dtype, apply_state)
        apply_state[(var_device, var_dtype)]["weight_decay_rate"] = tf.constant(
            self.weight_decay_rate, name="adam_weight_decay_rate"
        )

    def _decay_weights_op(self, var, learning_rate, apply_state):
        do_decay = self._do_use_weight_decay(var.name)
        if do_decay:
            return var.assign_sub(
                learning_rate * var * apply_state[(var.device, var.dtype.base_dtype)]["weight_decay_rate"],
                use_locking=self._use_locking,
            )
        return tf.no_op()

    def apply_gradients(self, grads_and_vars, name=None, **kwargs):
        grads, tvars = list(zip(*grads_and_vars))
        return super(AdamWeightDecay, self).apply_gradients(zip(grads, tvars), name=name, **kwargs)

    def _get_lr(self, var_device, var_dtype, apply_state):
        """Retrieves the learning rate with the given state."""
        if apply_state is None:
            return self._decayed_lr_t[var_dtype], {}

        apply_state = apply_state or {}
        coefficients = apply_state.get((var_device, var_dtype))
        if coefficients is None:
            coefficients = self._fallback_apply_state(var_device, var_dtype)
            apply_state[(var_device, var_dtype)] = coefficients

        return coefficients["lr_t"], dict(apply_state=apply_state)

    def _resource_apply_dense(self, grad, var, apply_state=None):
        lr_t, kwargs = self._get_lr(var.device, var.dtype.base_dtype, apply_state)
        decay = self._decay_weights_op(var, lr_t, apply_state)
        with tf.control_dependencies([decay]):
            return super(AdamWeightDecay, self)._resource_apply_dense(grad, var, **kwargs)

    def _resource_apply_sparse(self, grad, var, indices, apply_state=None):
        lr_t, kwargs = self._get_lr(var.device, var.dtype.base_dtype, apply_state)
        decay = self._decay_weights_op(var, lr_t, apply_state)
        with tf.control_dependencies([decay]):
            return super(AdamWeightDecay, self)._resource_apply_sparse(grad, var, indices, **kwargs)

    def get_config(self):
        config = super().get_config()
        config.update({"weight_decay_rate": self.weight_decay_rate})
        return config

    def _do_use_weight_decay(self, param_name):
        """Whether to use L2 weight decay for `param_name`."""
        if self.weight_decay_rate == 0:
            return False

        if self._include_in_weight_decay:
            for r in self._include_in_weight_decay:
                if re.search(r, param_name) is not None:
                    return True

        if self._exclude_from_weight_decay:
            for r in self._exclude_from_weight_decay:
                if re.search(r, param_name) is not None:
                    return False
        return True


def accuracy(output, target, topk=(1,)):
    """
    Computes the accuracy over the k top predictions
    for the specified values of k.
    This is based on:
    https://github.com/rwightman/pytorch-image-models/blob/master/timm/utils/metrics.py
    """
    maxk = max(topk)
    shape = target.shape
    batch_size = shape[0]

    if batch_size != shape[-1]:  # Make Sparse labels
        target = tf.argmax(target, -1, output_type=tf.int32)

    _, indices = tf.math.top_k(output, k=maxk, sorted=True)
    pred = tf.transpose(indices)
    correct = tf.math.equal(pred, target)
    out = [tf.reduce_sum(
        tf.cast(tf.reshape(correct[:k], [batch_size, -1]),
                tf.float32)) * 100. / batch_size for k in topk]
    return out[0] if len(topk) == 1 else out


def add_weight_decay(model, l2):
    # a utility function to add weight decay after the model is defined.
    if (l2 is None) or (l2 == 0.0):
        return

    # recursion inside the model
    def add_decay_loss(m, factor):
        if isinstance(m, tf.keras.Model):
            for layer in m.layers:
                add_decay_loss(layer, factor)
        else:
            for param in m.trainable_weights:
                with tf.keras.backend.name_scope('weight_regularizer'):
                    regularizer = lambda: tf.keras.regularizers.l2(factor)(param)
                    m.add_loss(regularizer)

    add_decay_loss(model, l2)


class RandomResizedCrop(tf.keras.layers.Layer):
    """
    A keras layer implementation that randomly adds resizes and crops images
    """

    def __init__(self, scale, ratio, **kwargs):
        super(RandomResizedCrop, self).__init__(**kwargs)
        # area-range of the cropped part: (min area, max area), uniform sampling
        self.scale = scale
        # aspect-ratio-range of the cropped part: (log min ratio, log max ratio), log-uniform sampling
        if ratio[0] == ratio[1] == 1:
            self.adjust_ratio = False
        else:
            self.adjust_ratio = True

        self.log_ratio = (tf.math.log(ratio[0]), tf.math.log(ratio[1]))

    def call(self, images, training=True):
        """

        :param images: Input Batch
        :param training: Do we train or not -> Boolean
        :return:
        """
        if training:
            batch_size = tf.shape(images)[0]
            height = tf.shape(images)[1]
            width = tf.shape(images)[2]

            # independently sampled scales and ratios for every image in the batch
            random_scales = tf.random.uniform(
                (batch_size,), self.scale[0], self.scale[1]
            )
            if self.adjust_ratio:
                random_ratios = tf.exp(
                    tf.random.uniform((batch_size,), self.log_ratio[0], self.log_ratio[1])
                )
                # corresponding height and widths, clipped to fit in the image
                new_heights = tf.clip_by_value(tf.sqrt(random_scales / random_ratios), 0, 1)
                new_widths = tf.clip_by_value(tf.sqrt(random_scales * random_ratios), 0, 1)
            else:
                new_widths = tf.clip_by_value(tf.sqrt(random_scales), 0, 1)
                new_heights = tf.identity(new_widths)

            # random anchors for the crop bounding boxes
            height_offsets = tf.random.uniform((batch_size,), 0, 1 - new_heights)
            width_offsets = tf.random.uniform((batch_size,), 0, 1 - new_widths)

            # assemble bounding boxes and crop
            bounding_boxes = tf.stack(
                [
                    height_offsets,
                    width_offsets,
                    height_offsets + new_heights,
                    width_offsets + new_widths,
                ],
                axis=1,
            )
            images = tf.image.crop_and_resize(
                images, bounding_boxes, tf.range(batch_size), (height, width)
            )

        return images


class RandomFill(tf.keras.layers.Layer):
    """
    A keras layer implementation that randomly adds black patches to images
    """

    def __init__(self, value, scale, ratio, **kwargs):
        super(RandomFill, self).__init__(**kwargs)
        # area-range of the cropped part: (min area, max area), uniform sampling
        self.scale = scale
        # aspect-ratio-range of the cropped part: (log min ratio, log max ratio), log-uniform sampling
        self.log_ratio = (tf.math.log(ratio[0]), tf.math.log(ratio[1]))

        self.fn = lambda a: fill_boxes(a, val=value)

    def call(self, images, training=True):
        """

        :param images: Input Batch
        :param training: Do we train or not -> Boolean
        :return:
        """
        if training:
            batch_size = tf.shape(images)[0]
            height = tf.shape(images)[1]
            width = tf.shape(images)[2]

            # independently sampled scales and ratios for every image in the batch
            random_scales = tf.random.uniform(
                (batch_size,), self.scale[0], self.scale[1]
            )
            random_ratios = tf.exp(
                tf.random.uniform((batch_size,), self.log_ratio[0], self.log_ratio[1])
            )

            # corresponding height and widths, clipped to fit in the image
            new_heights = tf.clip_by_value(tf.sqrt(random_scales / random_ratios), 0, 1)
            new_widths = tf.clip_by_value(tf.sqrt(random_scales * random_ratios), 0, 1)

            # random anchors for the crop bounding boxes
            height_offsets = tf.random.uniform((batch_size,), 0, 1 - new_heights)
            width_offsets = tf.random.uniform((batch_size,), 0, 1 - new_widths)

            # assemble bounding boxes and crop
            bounding_boxes = tf.stack(
                [
                    tf.math.round(tf.math.multiply(height_offsets, tf.cast(height, tf.float32))),
                    tf.math.round(tf.math.multiply(width_offsets, tf.cast(width, tf.float32))),
                    tf.math.round(tf.math.multiply(height_offsets + new_heights, tf.cast(height, tf.float32))),
                    tf.math.round(tf.math.multiply(width_offsets + new_widths, tf.cast(width, tf.float32))),
                ],
                axis=1,
            )

            images = tf.map_fn(self.fn, [bounding_boxes, images],
                               fn_output_signature=tf.float32)

        return images


class RandomSensitivityHole(tf.keras.layers.Layer):
    """
    A keras layer implementation that randomly adds a Gaussian sensitivity hole known from MCP detectors
    """

    def __init__(self, scale, **kwargs):
        super(RandomSensitivityHole, self).__init__(**kwargs)
        # area-range of the cropped part: (min area, max area), uniform sampling
        self.scale = np.array(scale)

    def call(self, images, training=True):
        """

        :param images: Input Batch
        :param training: Do we train or not -> Boolean
        :return:
        """
        if training:
            batch_size = tf.shape(images)[0]
            height = tf.shape(images)[1]
            width = tf.shape(images)[2]

            # independently sampled scales and ratios for every image in the batch
            random_scales = tf.random.uniform(
                (batch_size,), self.scale[0], self.scale[1]
            )
            # corresponding height and widths, clipped to fit in the image
            ratios = tf.cast(tf.ones([batch_size]), random_scales.dtype)
            new_heights = tf.clip_by_value(tf.sqrt(random_scales / ratios), 0, 1)
            new_widths = tf.clip_by_value(tf.sqrt(random_scales * ratios), 0, 1)

            # random anchors for the crop bounding boxes
            height_offsets = tf.random.uniform((batch_size,), 0, 1 - new_heights)
            width_offsets = tf.random.uniform((batch_size,), 0, 1 - new_widths)

            # assemble bounding boxes and crop .. [y_start, x_start, y_end, x_end]
            bounding_boxes = tf.stack(
                [
                    tf.math.round(tf.math.multiply(height_offsets, tf.cast(height, tf.float32))),
                    tf.math.round(tf.math.multiply(width_offsets, tf.cast(width, tf.float32))),
                    tf.math.round(tf.math.multiply(height_offsets + new_heights, tf.cast(height, tf.float32))),
                    tf.math.round(tf.math.multiply(width_offsets + new_widths, tf.cast(width, tf.float32))),
                ],
                axis=1,
            )

            images = tf.map_fn(blur_boxes, [bounding_boxes, images],
                               fn_output_signature=tf.float32)

        return images


class RandomScale(tf.keras.layers.Layer):
    """
    A keras layer implementation that randomly scales patches of images
    """

    def __init__(self, scale, ratio, **kwargs):
        super(RandomScale, self).__init__(**kwargs)
        # area-range of the cropped part: (min area, max area), uniform sampling
        self.scale = np.array(scale)
        # aspect-ratio-range of the cropped part: (log min ratio, log max ratio), log-uniform sampling
        self.log_ratio = (tf.math.log(ratio[0]), tf.math.log(ratio[1]))

        self.fn = lambda a: scale_boxes(a)

    def call(self, images, training=True):
        """

        :param images: Input Batch
        :param training: Do we train or not -> Boolean
        :return:
        """
        if training:
            batch_size = tf.shape(images)[0]
            height = tf.shape(images)[1]
            width = tf.shape(images)[2]

            # independently sampled scales and ratios for every image in the batch
            random_scales = tf.random.uniform(
                (batch_size,), self.scale[0], self.scale[1]
            )
            random_ratios = tf.exp(
                tf.random.uniform((batch_size,), self.log_ratio[0], self.log_ratio[1])
            )

            # corresponding height and widths, clipped to fit in the image
            new_heights = tf.clip_by_value(tf.sqrt(random_scales / random_ratios), 0, 1)
            new_widths = tf.clip_by_value(tf.sqrt(random_scales * random_ratios), 0, 1)

            # random anchors for the crop bounding boxes
            height_offsets = tf.random.uniform((batch_size,), 0, 1 - new_heights)
            width_offsets = tf.random.uniform((batch_size,), 0, 1 - new_widths)

            # assemble bounding boxes and crop ....
            bounding_boxes = tf.stack(
                [
                    tf.math.round(tf.math.multiply(height_offsets, tf.cast(height, tf.float32))),
                    tf.math.round(tf.math.multiply(width_offsets, tf.cast(width, tf.float32))),
                    tf.math.round(tf.math.multiply(height_offsets + new_heights, tf.cast(height, tf.float32))),
                    tf.math.round(tf.math.multiply(width_offsets + new_widths, tf.cast(width, tf.float32))),
                ],
                axis=1,
            )

            images = tf.map_fn(self.fn, [bounding_boxes, images],
                               fn_output_signature=tf.float32)

        return images


class Normalize(tf.keras.layers.Layer):
    """
    A keras layer implementation that normalizes batches of images
    """

    def __init__(self, new_max=1., new_min=0., use_image_normalization=True,
                 **kwargs):
        super(Normalize, self).__init__(**kwargs)

        # self.fn = lambda a: normalize_img_single(a, new_max=new_max,
        #                                          new_min=new_min,
        #                                          use_image_normalization=use_image_normalization)
        self.fn = lambda x: normalize_img(x, new_max=new_max,
                                          new_min=new_min,
                                          use_image_normalization=use_image_normalization)

    def call(self, images, training=True):
        """

        :param images: Input Batch
        :param training: Do we train or not -> Boolean
        :return:
        """
        return self.fn(images)


class RandomJitterAffine(tf.keras.layers.Layer):
    """
    A keras layer implementation that applies Random Jitter Transformations
    """

    def __init__(self, brightness=0., jitter=0., **kwargs):
        super(RandomJitterAffine, self).__init__(**kwargs)

        self.brightness = brightness
        self.jitter = jitter

    def call(self, images, training=True):
        """

        :param images: Input Batch
        :param training: Do we train or not -> Boolean
        :return:
        """
        if training:
            batch_size = tf.shape(images)[0]

            brightness_scales = 1 + tf.random.uniform(
                (batch_size, 1, 1, 1), minval=-self.brightness, maxval=self.brightness
            )
            jitter_matrices = tf.random.uniform(
                (batch_size, 1, 1, 1), minval=-self.jitter, maxval=self.jitter
            )

            jitter_transforms = (
                    tf.eye(1, batch_shape=[batch_size, 1]) * brightness_scales
                    + jitter_matrices
            )
            images = tf.matmul(images, tf.cast(jitter_transforms, images.dtype))
        return images


class RandomNoise(tf.keras.layers.Layer):
    """
    A keras layer implementation that applies Random Normal and Poisson Noise to an image
    """

    def __init__(self, lam=4, percentile=75, std=0.25, **kwargs):
        """

        :param lam: Lambda for the Poisson Noise
        :param percentile: Percentile that is used as mean for the Normal Noise
        :param std: Std for the Normal Noise
        :param kwargs: kwargs
        """
        super(RandomNoise, self).__init__(**kwargs)
        self.lam = lam
        self.percentile = percentile
        self.std = std

    def call(self, images, training=True):
        """

        :param images: Input Batch
        :param training: Do we train or not -> Boolean
        :return:
        """
        if training:
            shape_img = tf.shape(images)
            images *= tf.cast(tf.random.poisson(shape=shape_img, lam=self.lam) >= 1, images.dtype)
            images += tf.cast(tf.math.exp(tf.random.normal(shape_img,
                                                           tf.math.log(tfp.stats.percentile(
                                                               tf.cast(images, tf.float32), self.percentile)),
                                                           self.std)), images.dtype)
        return images


@tf.function
def get_inp_idx(inp_img):
    """

    :param inp_img:
    :return:
    """
    inp, img = inp_img[0], inp_img[1]
    img = tf.cast(img, tf.float32)

    a, b, c, d = inp[0], inp[1], inp[2], inp[3]

    aa = tf.range(tf.squeeze(a), tf.squeeze(c))
    bb = tf.range(tf.squeeze(b), tf.squeeze(d))

    aa, bb = aa[None, :, None], bb[:, None, None]
    ind = tf.concat([aa + tf.zeros_like(bb),
                     tf.zeros_like(aa) + bb], axis=2)

    ind = tf.cast(tf.reshape(ind, [-1, 2]), tf.int64)
    ind_shape = tf.shape(ind)
    return img, ind, ind_shape


@tf.function
def fill_boxes(inp_img, val=0.):
    """

    :param inp_img:
    :param val:
    :return:
    """
    img, ind, ind_shape = get_inp_idx(inp_img)
    update_shape = (ind_shape[0], 1)
    upd = tf.multiply(tf.ones(update_shape, dtype=img.dtype), val)

    x = tf.tensor_scatter_nd_update(img, tf.cast(ind, tf.int64), upd)
    return x


@tf.function
def scale_boxes(inp_img):
    """

    :param inp_img:
    :return:
    """
    img, ind, ind_shape = get_inp_idx(inp_img)
    # scale with a skewed distribution between various roots
    power = tf.math.exp(tf.random.normal([1], tf.math.log(0.2), 0.4))
    upd_pow = tf.math.pow(tf.cast(
        tf.gather_nd(img, ind),
        dtype=img.dtype), power)
    x = tf.tensor_scatter_nd_update(img, ind, upd_pow)
    return x


@tf.function
def two_dim_gaussian_fun(u1, u2, o1, o2, indexes):
    """

    :param u1: mean 1
    :param u2: mean 2
    :param o1: sigma 1
    :param o2: sigma 2
    :param indexes: the indexes
    :return: 2-d gaussian
    """
    # blank = tf.ones([size * size])
    # X, Y = tf.meshgrid(tf.range(size), tf.range(size))
    first_term = (tf.cast(indexes[:, 0], u1.dtype) - u1) ** 2 / 2 * o1 ** 2
    second_term = (tf.cast(indexes[:, 1], u2.dtype) - u2) ** 2 / 2 * o2 ** 2
    ff = tf.exp(-(first_term + second_term))

    return tf.expand_dims(1 - ff, -1)


@tf.function
def normalize_img(x, new_max=1., new_min=0.,
                  use_image_normalization=True):
    """
    Linearly interpolate a batch of images between "new_max" and "new_min"
    :param x: Input -> Will be casted to float32
    :param new_max: New maximum of x
    :param new_min: New minimum of x
    :param use_image_normalization: centering around zero, adjusting std to be one
    :return: Scaled and casted version of x
    """

    x_shape = tf.shape(x)
    if len(x_shape) < 4:
        x = tf.expand_dims(x, 0)
        if len(x_shape) == 2:
            x = tf.expand_dims(x, -1)

    x_shape = tf.shape(x)
    bs = x_shape[0]
    x_cast = tf.cast(x, tf.float32)

    outer_dim = tf.math.reduce_prod(x_shape[1:])
    x_cast_reshaped = tf.reshape(x_cast, [bs, outer_dim])
    current_min = tf.repeat(
        tf.expand_dims(
            tf.math.reduce_min(x_cast_reshaped, axis=-1),
            -1), outer_dim, axis=-1)
    current_max = tf.repeat(
        tf.expand_dims(
            tf.math.reduce_max(x_cast_reshaped, axis=-1),
            -1), outer_dim, axis=-1)

    if use_image_normalization:
        current_mean = tf.repeat(
            tf.expand_dims(
                tf.math.reduce_mean(x_cast_reshaped, axis=-1),
                -1), outer_dim, axis=-1)
        current_std = tf.repeat(
            tf.expand_dims(
                tf.math.reduce_std(x_cast_reshaped, axis=-1),
                -1), outer_dim, axis=-1)
        updates = 1.0 / tf.math.sqrt(tf.cast(outer_dim, tf.float32))
        cond = tf.math.greater(current_std, updates)
        adjusted_stddev = tf.where(cond, current_std, updates)
        scaled_x = (x_cast_reshaped - current_mean) / adjusted_stddev

    else:
        new_max_cast = tf.broadcast_to(tf.cast(new_max, tf.float32), [bs, outer_dim])
        new_min_cast = tf.broadcast_to(tf.cast(new_min, tf.float32), [bs, outer_dim])
        scaled_x = new_min_cast + (new_max_cast - new_min_cast) * (
                x_cast_reshaped - current_min) / (current_max - current_min)

    scaled_x = tf.reshape(scaled_x, x_shape)
    return tf.cast(scaled_x, tf.float32)


@tf.function
def blur_boxes(inp_img):
    """

    :param inp_img:
    :return:
    """
    inp, img = inp_img[0], inp_img[1]
    img = tf.cast(img, tf.float32)
    shape_ = tf.cast(tf.shape(img), img.dtype)

    a, b, c, d = inp[0], inp[1], inp[2], inp[3]

    u1 = tf.reduce_max([a, c]) - 0.5 * tf.math.abs(a - c)
    u2 = tf.reduce_max([b, d]) - 0.5 * tf.math.abs(b - d)
    o1 = shape_[0] / tf.math.abs(a - c) / 30
    o2 = shape_[0] / tf.math.abs(b - d) / 30

    aa = tf.range(tf.squeeze(a), tf.squeeze(c))
    bb = tf.range(tf.squeeze(b), tf.squeeze(d))

    aa, bb = aa[None, :, None], bb[:, None, None]
    ind = tf.concat([aa + tf.zeros_like(bb),
                     tf.zeros_like(aa) + bb], axis=2)

    ind = tf.cast(tf.reshape(ind, [-1, 2]), tf.int64)
    # print("\n--------------------------------\n",
    #       inp, "\n", ind,
    #       "\n--------------------------------\n")
    gauss = two_dim_gaussian_fun(u1, u2, o1, o2, ind)
    # print(inp, "\n", u1, "\n", u2, "\n", o1, "\n", o2)
    upd = tf.math.multiply(tf.cast(
        tf.gather_nd(img, ind),
        dtype=img.dtype), gauss)

    x = tf.tensor_scatter_nd_update(img, ind, upd)
    return x


def gpu_augmenter(in_shape, brightness=0.25, jitter=0.2, scale=(0.5, 1.0), ratio=(1., 1.),
                  fill_scale=(0.05, 0.75), fill_ratio=(0.25, 2.5), fill_value=0., strength=0.4,
                  use_image_normalization=True):
    """
    Image augmentation module
    :param in_shape:
    :param brightness:
    :param jitter:
    :param scale:
    :param ratio:
    :param fill_scale:
    :param fill_ratio:
    :param fill_value:
    :param strength:
    :param use_image_normalization:
    :return:
    """
    return tf.keras.Sequential(
        [
            tf.keras.Input(shape=in_shape),
            RandomNoise(name="RandomNoise"),
            tf.keras.layers.RandomRotation(strength, fill_mode="constant",
                                           fill_value=fill_value,
                                           name="RandomRotationOne"),
            RandomScale(scale=fill_scale, ratio=fill_ratio, name="RandomScale"),
            RandomJitterAffine(brightness, jitter, name="RandomJitterAffine"),
            tf.keras.layers.RandomRotation(strength, fill_mode="constant",
                                           fill_value=fill_value,
                                           name="RandomRotationTwo"),
            RandomResizedCrop(scale=scale, ratio=ratio, name="RandomResizedCrop"),
            RandomSensitivityHole(scale=fill_scale, name="RandomSensitivityHole"),
            tf.keras.layers.RandomFlip("horizontal", name="RandomFlip"),
            tf.keras.layers.RandomRotation(strength, fill_mode="constant",
                                           fill_value=fill_value,
                                           name="RandomRotationThree"),
            tf.keras.layers.RandomTranslation(strength / 3, strength / 3,
                                              fill_mode="constant",
                                              fill_value=fill_value,
                                              name="RandomTranslation"),
            Normalize(use_image_normalization=use_image_normalization,
                      name="Normalization")
        ]
    )

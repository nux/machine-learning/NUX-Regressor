# coding=utf-8
# Copyright 2020 The SimCLR Authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific simclr governing permissions and
# limitations under the License.
# ==============================================================================
"""Data preprocessing and augmentation."""

import io
import math
import scatman
import functools
import matplotlib
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
import tensorflow_probability as tfp
from itertools import product

tfd = tfp.distributions


def check_split_string(splstr, info):
    """
    This extracts a multiplier factor from the split list string
    :param splstr: The split string passed to the training routine
    :param info: The info frame provided by Squirrel
    :return:
    """
    assert isinstance(splstr, str)

    # Get the index
    if "[" in splstr and "]" in splstr:
        ty = splstr[:splstr.find("[")]
        idx_str = splstr[splstr.find("[") + 1:splstr.find("]")]
        if ":" == idx_str[0]:
            first = True
            val_str = idx_str[1:]
        else:
            first = False
            val_str = idx_str[:-1]

        if "%" in splstr:
            factor = int(val_str.replace("%", "")) / 100
            if not first:
                factor = 1 - factor
            num_examples = round(info.splits[ty].num_examples * factor)
        else:
            num_examples = int(val_str)
            if not first:
                num_examples = info.splits[ty].num_examples - num_examples
            # factor = num_examples / info.splits[ty].num_examples

        return num_examples
    else:
        raise LookupError("'[' and ']' not in splstr")


def random_apply(func, p, x):
    """Randomly apply function func to x with probability p."""
    return tf.cond(
        tf.less(
            tf.random.uniform([], minval=0, maxval=1, dtype=tf.float32),
            tf.cast(p, tf.float32)), lambda: func(x), lambda: x)


def random_brightness(image, max_delta, impl='simclrv2'):
    """A multiplicative vs additive change of brightness."""
    if impl == 'simclrv2':
        factor = tf.random.uniform([tf.shape(image)[0]], tf.maximum(1.0 - max_delta, 0),
                                   1.0 + max_delta)
        image = tf.transpose(tf.transpose(image) * factor)
    elif impl == 'simclrv1':
        image = tf.image.random_brightness(image, max_delta=max_delta)
    else:
        raise ValueError('Unknown impl {} for random brightness.'.format(impl))
    return image


def to_grayscale(image, keep_channels=True):
    image = tf.image.rgb_to_grayscale(image)
    if keep_channels:
        image = tf.tile(image, [1, 1, 1, 3])
    return image


def distorted_bounding_box_crop(image,
                                bbox,
                                min_object_covered=0.1,
                                aspect_ratio_range=(0.75, 1.33),
                                area_range=(0.05, 1.0),
                                max_attempts=100,
                                scope=None):
    """Generates cropped_image using one of the bboxes randomly distorted.
    See `tf.image.sample_distorted_bounding_box` for more documentation.
    Args:
      image: `Tensor` of image data.
      bbox: `Tensor` of bounding boxes arranged `[1, num_boxes, coords]`
          where each coordinate is [0, 1) and the coordinates are arranged
          as `[ymin, xmin, ymax, xmax]`. If num_boxes is 0 then use the whole
          image.
      min_object_covered: An optional `float`. Defaults to `0.1`. The cropped
          area of the image must contain at least this fraction of any bounding
          box supplied.
      aspect_ratio_range: An optional list of `float`s. The cropped area of the
          image must have an aspect ratio = width / height within this range.
      area_range: An optional list of `float`s. The cropped area of the image
          must contain a fraction of the supplied image within in this range.
      max_attempts: An optional `int`. Number of attempts at generating a cropped
          region of the image of the specified constraints. After `max_attempts`
          failures, return the entire image.
      scope: Optional `str` for name scope.
    Returns:
      (cropped image `Tensor`, distorted bbox `Tensor`).
    """
    with tf.name_scope(scope or 'distorted_bounding_box_crop'):
        shape = tf.shape(image)
        sample_distorted_bounding_box = tf.image.sample_distorted_bounding_box(
            shape,
            bounding_boxes=bbox,
            min_object_covered=min_object_covered,
            aspect_ratio_range=aspect_ratio_range,
            area_range=area_range,
            max_attempts=max_attempts,
            use_image_if_no_bounding_boxes=True)
        bbox_begin, bbox_size, _ = sample_distorted_bounding_box

        # Crop the image to the specified bounding box.
        offset_y, offset_x, _ = tf.unstack(bbox_begin)
        target_height, target_width, _ = tf.unstack(bbox_size)
        image = tf.image.crop_to_bounding_box(
            image, offset_y, offset_x, target_height, target_width)

        return image


def crop_and_resize(image, height, width):
    """Make a random crop and resize it to height `height` and width `width`.
    Args:
      image: Tensor representing the image.
      height: Desired image height.
      width: Desired image width.
    Returns:
      A `height` x `width` x channels Tensor holding a random crop of `image`.
    """
    bbox = tf.constant([0.0, 0.0, 1.0, 1.0], dtype=tf.float32, shape=[1, 1, 4])
    aspect_ratio = width / height
    image = distorted_bounding_box_crop(
        image,
        bbox,
        min_object_covered=0.1,
        aspect_ratio_range=(3. / 4 * aspect_ratio, 4. / 3. * aspect_ratio),
        area_range=(0.08, 1.0),
        max_attempts=100,
        scope=None)
    return tf.image.resize([image], [height, width],
                           method=tf.image.ResizeMethod.BICUBIC)[0]


def random_crop_with_resize(image, height, width, p=1.0):
    """Randomly crop and resize an image.
    Args:
      image: `Tensor` representing an image of arbitrary size.
      height: Height of output image.
      width: Width of output image.
      p: Probability of applying this transformation.
    Returns:
      A preprocessed image `Tensor`.
    """

    def _transform(image):  # pylint: disable=missing-docstring
        image = crop_and_resize(image, height, width)
        return image

    return random_apply(_transform, p=p, x=image)


def color_jitter(image, strength, random_order=True, impl='simclrv2',
                 brightness_flag=True, contrast_flag=True,
                 saturation_flag=True, hue_flag=True):
    """Distorts the color of the image.

    Args:
      image: The input image tensor.
      strength: the floating number for the strength of the color augmentation.
      random_order: A bool, specifying whether to randomize the jittering order.
      impl: 'simclrv1' or 'simclrv2'.  Whether to use simclrv1 or simclrv2's
          version of random brightness.
      brightness_flag: Boolean if brightness should be adjusted
      contrast_flag: Boolean if contrast should be adjusted
      saturation_flag: Boolean if saturation should be adjusted
      hue_flag: Boolean if hue should be adjusted

    Returns:
      The distorted image tensor.
    """
    if brightness_flag:
        brightness = strength
    else:
        brightness = 0
    if contrast_flag:
        contrast = strength
    else:
        contrast = 0
    if saturation_flag:
        saturation = strength
    else:
        saturation = 0
    if hue_flag:
        hue = strength
    else:
        hue = 0

    if random_order:
        return color_jitter_rand(
            image, brightness, contrast, saturation, hue, impl=impl)
    else:
        return color_jitter_nonrand(
            image, brightness, contrast, saturation, hue, impl=impl)


def color_jitter_nonrand(image,
                         brightness=0,
                         contrast=0,
                         saturation=0,
                         hue=0,
                         impl='simclrv2'):
    """Distorts the color of the image (jittering order is fixed).

    Args:
      image: The input image tensor.
      brightness: A float, specifying the brightness for color jitter.
      contrast: A float, specifying the contrast for color jitter.
      saturation: A float, specifying the saturation for color jitter.
      hue: A float, specifying the hue for color jitter.
      impl: 'simclrv1' or 'simclrv2'.  Whether to use simclrv1 or simclrv2's
          version of random brightness.

    Returns:
      The distorted image tensor.
    """
    with tf.name_scope('distort_color'):
        def apply_transform(i, x, brightness, contrast, saturation, hue):
            """Apply the i-th transformation."""
            if brightness != 0 and i == 0:
                x = random_brightness(x, max_delta=brightness, impl=impl)
            elif contrast != 0 and i == 1:
                x = tf.image.random_contrast(
                    x, lower=1 - contrast, upper=1 + contrast)
            elif saturation != 0 and i == 2:
                x = tf.image.random_saturation(
                    x, lower=1 - saturation, upper=1 + saturation)
            elif hue != 0:
                x = tf.image.random_hue(x, max_delta=hue)
            return x

        for i in range(4):
            image = apply_transform(i, image, brightness, contrast, saturation, hue)
            image = tf.clip_by_value(image, 0., 1.)
        return image


def color_jitter_rand(image,
                      brightness=0,
                      contrast=0,
                      saturation=0,
                      hue=0,
                      impl='simclrv2'):
    """Distorts the color of the image (jittering order is random).

    Args:
      image: The input image tensor.
      brightness: A float, specifying the brightness for color jitter.
      contrast: A float, specifying the contrast for color jitter.
      saturation: A float, specifying the saturation for color jitter.
      hue: A float, specifying the hue for color jitter.
      impl: 'simclrv1' or 'simclrv2'.  Whether to use simclrv1 or simclrv2's
          version of random brightness.

    Returns:
      The distorted image tensor.
    """
    with tf.name_scope('distort_color'):
        def apply_transform(i, x):
            """Apply the i-th transformation."""

            def brightness_foo():
                if brightness == 0:
                    return x
                else:
                    return random_brightness(x, max_delta=brightness, impl=impl)

            def contrast_foo():
                if contrast == 0:
                    return x
                else:
                    return tf.image.random_contrast(x, lower=1 - contrast, upper=1 + contrast)

            def saturation_foo():
                if saturation == 0:
                    return x
                else:
                    return tf.image.random_saturation(
                        x, lower=1 - saturation, upper=1 + saturation)

            def hue_foo():
                if hue == 0:
                    return x
                else:
                    return tf.image.random_hue(x, max_delta=hue)

            x = tf.cond(tf.less(i, 2),
                        lambda: tf.cond(tf.less(i, 1), brightness_foo, contrast_foo),
                        lambda: tf.cond(tf.less(i, 3), saturation_foo, hue_foo))
            return x

        perm = tf.random.shuffle(tf.range(4))
        for i in range(4):
            image = apply_transform(perm[i], image)
            image = tf.clip_by_value(image, 0., 1.)
        return image


def random_color_jitter(image, p=1.0, strength=0.125, impl='simclrv2'):
    def _transform(image):
        color_jitter_t = functools.partial(
            color_jitter, strength=strength, impl=impl, hue_flag=False)
        # return random_apply(to_grayscale, p=0.2, x=image)
        return color_jitter_t(image)

    return random_apply(_transform, p=p, x=image)


def expand_channels(img, label, to_gray=False,
                    expand_to_last_dim=True, to_rgb=True):
    if expand_to_last_dim:
        img = tf.expand_dims(img, -1)
    if to_gray:
        img = tf.image.rgb_to_grayscale(img)
    if to_rgb:
        img = tf.image.grayscale_to_rgb(img)
    return img, label


def expand_channels_sobel(img, lbl):
    img_conc = tf.expand_dims(img, -1)
    img = tf.expand_dims(tf.expand_dims(img, 0), -1)

    sobel_images = tf.squeeze(tf.image.sobel_edges(img))
    img = tf.concat([img_conc, sobel_images], axis=-1)
    return img, lbl


def _compute_crop_shape(
        image_height, image_width, aspect_ratio, crop_proportion):
    """Compute aspect ratio-preserving shape for central crop.
    The resulting shape retains `crop_proportion` along one side and a proportion
    less than or equal to `crop_proportion` along the other side.
    Args:
      image_height: Height of image to be cropped.
      image_width: Width of image to be cropped.
      aspect_ratio: Desired aspect ratio (width / height) of output.
      crop_proportion: Proportion of image to retain along the less-cropped side.
    Returns:
      crop_height: Height of image after cropping.
      crop_width: Width of image after cropping.
    """
    image_width_float = tf.cast(image_width, tf.float64)
    image_height_float = tf.cast(image_height, tf.float64)

    def _requested_aspect_ratio_wider_than_image():
        crop_height = tf.cast(
            tf.math.rint(crop_proportion / aspect_ratio * image_width_float),
            tf.int32)
        crop_width = tf.cast(
            tf.math.rint(crop_proportion * image_width_float), tf.int32)
        return crop_height, crop_width

    def _image_wider_than_requested_aspect_ratio():
        crop_height = tf.cast(
            tf.math.rint(crop_proportion * image_height_float), tf.int32)
        crop_width = tf.cast(
            tf.math.rint(crop_proportion * aspect_ratio * image_height_float),
            tf.int32)
        return crop_height, crop_width

    return tf.cond(
        aspect_ratio > image_width_float / image_height_float,
        _requested_aspect_ratio_wider_than_image,
        _image_wider_than_requested_aspect_ratio)


def center_crop(image, height, width, crop_proportion):
    """Crops to center of image and rescales to desired size.
    Args:
      image: Image Tensor to crop.
      height: Height of image to be cropped.
      width: Width of image to be cropped.
      crop_proportion: Proportion of image to retain along the less-cropped side.
    Returns:
      A `height` x `width` x channels Tensor holding a central crop of `image`.
    """
    shape = tf.shape(image)
    image_height = shape[0]
    image_width = shape[1]
    crop_height, crop_width = _compute_crop_shape(
        image_height, image_width, height / width, crop_proportion)
    offset_height = ((image_height - crop_height) + 1) // 2
    offset_width = ((image_width - crop_width) + 1) // 2
    image = tf.image.crop_to_bounding_box(
        image, offset_height, offset_width, crop_height, crop_width)

    image = tf.image.resize([image], [height, width],
                            method=tf.image.ResizeMethod.BICUBIC)[0]

    return image


@tf.function
def center_cutout(img, label):
    # This cuts out the central part of the image so that
    # the height and width of the cropped area are fully
    # within the diffraction pattern.
    image = tf.image.central_crop(img, math.cos(math.pi / 4))
    return image, label


# @tf.function
def det_hole(img, label, args):
    # This adds a detector hole
    # Empirically derived for the given dataset with an image size of 256
    shape_ = args.image_size
    bs = args.batch_size
    pos_hole_rel_1 = np.round(np.array([110, 151, 116, 160]) / 256. * shape_).astype(int)
    pos_hole_rel_2 = np.round(np.array([129, 133, 80, 116]) / 256. * shape_).astype(int)
    pos_hole_rel_3 = np.round(np.array([100, 110, 125, 128]) / 256. * shape_).astype(int)
    pos_hole_rel_4 = np.round(np.array([151, 165, 122, 130]) / 256. * shape_).astype(int)

    indices_1 = list(product(np.arange(bs),
                             np.arange(pos_hole_rel_1[0], pos_hole_rel_1[1]),
                             np.arange(pos_hole_rel_1[2], pos_hole_rel_1[3]),
                             ))

    indices_2 = list(product(np.arange(bs),
                             np.arange(pos_hole_rel_2[0], pos_hole_rel_2[1]),
                             np.arange(pos_hole_rel_2[2], pos_hole_rel_2[3]),
                             ))

    indices_3 = list(product(np.arange(bs),
                             np.arange(pos_hole_rel_3[0], pos_hole_rel_3[1]),
                             np.arange(pos_hole_rel_3[2], pos_hole_rel_3[3]),
                             ))

    indices_4 = list(product(np.arange(bs),
                             np.arange(pos_hole_rel_4[0], pos_hole_rel_4[1]),
                             np.arange(pos_hole_rel_4[2], pos_hole_rel_4[3]),
                             ))

    upd_len = len(indices_1) + len(indices_2) + len(indices_3) + len(indices_4)
    image = tf.tensor_scatter_nd_update(tf.squeeze(img),
                                        indices_1 + indices_2 + indices_3 + indices_4,
                                        tf.constant(0, shape=[upd_len], dtype=img.dtype))

    return tf.expand_dims(image, -1), label


@tf.function
def normalize_img(x, lbl=None, new_max=1., new_min=0.,
                  use_image_normalization=True, to_uint8=False,
                  use_log=False):
    """
    Linearly interpolate a batch of images between "new_max" and "new_min"
    :param x: Input -> Will be casted to float32
    :param lbl: Label input in order to be used with tfds
    :param new_max: New maximum of x
    :param new_min: New minimum of x
    :param use_image_normalization: centering around zero, adjusting std to be one
    :param to_uint8: convert to uint8, scale between [0, 255]
    :param use_log: Apply natural log before standardization
    :return: Scaled and casted version of x
    """

    x_shape = tf.shape(x)
    bs = x_shape[0]
    x_cast = tf.cast(x, tf.float32)

    if use_log:
        x_cast = tf.math.log(x_cast)

    outer_dim = tf.math.reduce_prod(x_shape[1:])
    x_cast_reshaped = tf.reshape(x_cast, [bs, outer_dim])
    current_min = tf.repeat(
        tf.expand_dims(
            tf.math.reduce_min(x_cast_reshaped, axis=-1),
            -1), outer_dim, axis=-1)
    current_max = tf.repeat(
        tf.expand_dims(
            tf.math.reduce_max(x_cast_reshaped, axis=-1),
            -1), outer_dim, axis=-1)

    if to_uint8:
        max_8 = tf.broadcast_to(tf.cast(255, tf.float32), [bs, outer_dim])
        scaled_x = max_8 * (x_cast_reshaped - current_min) / (current_max - current_min)
        scaled_x = tf.reshape(scaled_x, x_shape)
        return tf.image.convert_image_dtype(scaled_x, dtype=tf.uint8), lbl

    if use_image_normalization:
        current_mean = tf.repeat(
            tf.expand_dims(
                tf.math.reduce_mean(x_cast_reshaped, axis=-1),
                -1), outer_dim, axis=-1)
        current_std = tf.repeat(
            tf.expand_dims(
                tf.math.reduce_std(x_cast_reshaped, axis=-1),
                -1), outer_dim, axis=-1)
        updates = 1.0 / tf.math.sqrt(tf.cast(outer_dim, tf.float32))
        cond = tf.math.greater(current_std, updates)
        adjusted_stddev = tf.where(cond, current_std, updates)
        scaled_x = (x_cast_reshaped - current_mean) / adjusted_stddev

    else:
        new_max_cast = tf.broadcast_to(tf.cast(new_max, tf.float32), [bs, outer_dim])
        new_min_cast = tf.broadcast_to(tf.cast(new_min, tf.float32), [bs, outer_dim])
        scaled_x = new_min_cast + (new_max_cast - new_min_cast) * \
                   (x_cast_reshaped - current_min) / (current_max - current_min)

    scaled_x = tf.reshape(scaled_x, x_shape)
    return tf.cast(scaled_x, tf.float32), lbl


@tf.function
def resize_img(x_, img_dims_out, batched=True):
    _in_shape = tf.shape(x_)
    # print(_in_shape)
    out_tuple = (img_dims_out[0], img_dims_out[1])
    if batched:
        _in_size = [_in_shape[1], _in_shape[2]]
        _in_bs = _in_shape[0]
    else:
        _in_size = [_in_shape[0], _in_shape[1]]
        _in_bs = None

    # if the min of in_size is still larger than the max of the desired output size
    # then do zoom in the image for resizing, but downsize it so that the subsequent crop
    # gets as much image information as possible
    # dim_in = tf.reduce_max(_in_size)
    dim_out = tf.reduce_max(out_tuple)

    scale_in = tf.reduce_max(_in_size) / tf.reduce_min(_in_size)
    rescale = tf.math.ceil(
        tf.math.multiply(tf.cast(dim_out, tf.float32),
                         tf.cast(scale_in, tf.float32)))
    x_ = tf.image.resize(x_, (rescale, rescale), preserve_aspect_ratio=True,
                         method=tf.image.ResizeMethod.NEAREST_NEIGHBOR)

    if batched:
        crop_tuple = (_in_bs, img_dims_out[0], img_dims_out[1], img_dims_out[2])
    else:
        crop_tuple = img_dims_out
    x_ = tf.image.random_crop(x_, crop_tuple)
    return x_


@tf.function
def base_augment(images, labels,
                 img_dims,
                 crop=True,
                 color_distort=True,
                 flip=True,
                 mode="eval",
                 method="simclr",
                 resize=True):
    """
    From:
    Atito, S., Awais, M. & Kittler, J.
    SiT: Self-supervised vIsion Transformer. (2021)

    Simple data augmentation techniques are applied during the self-supervised training.
    We found that to learn low- level features as well as high-level semantic information,
    aggressive data augmentation like MixUp [44] and Auto- Augment [45] hurts the training,
    specially with the objective functions in hand. Therefore, we used only cropping,
    colour jittering and horizontal flipping by selecting a random patch from the image
    and resizing it to 224 × 224 with a random horizontal flip. After data augmentation,
    image distortion techniques described in Section 3 are applied to the perturbed
    image and the network is optimised together with the rotation prediction and
    contrastive learning to reconstruct the image after distortion.

    Args:
      images: `Tensor` representing an image of arbitrary size.
      labels: `Tensor` representing the labels of arbitrary size. (Not needed during pretrain)
      img_dims: Dimensions of the output image.
      crop: Whether to crop the image.
      color_distort: Whether to random color jitter
      flip: Whether or not to flip left and right of an image.
      mode: "pretrain_train", "pretrain_eval", "train", "eval", or "gpu
      method: either "randaugment", or "simclr"
      resize: should we resize ... this is either done on the batch (non-imagenet)
                of individually (imagenet) outside of this routine

    Returns:
      A preprocessed image `Tensor`.
    """

    @tf.function
    def simclr_augment(x_):
        if crop:
            if "train" in mode:
                x_ = tf.map_fn(lambda xx: random_crop_with_resize(xx, img_dims[0], img_dims[1]), x_)
            else:
                # Crop Proportion of 1, see arxiv:2106.09681
                x_ = tf.map_fn(lambda xx: center_crop(xx, img_dims[0], img_dims[1],
                                                      crop_proportion=1.0), x_)
        if flip:
            x_ = tf.image.random_flip_left_right(x_)
        if color_distort:
            x_ = random_color_jitter(x_, strength=.125)
        x_ = tf.clip_by_value(x_, 0., 1.)
        return x_

    # Don't do anything if we do augmentation on GPU
    if mode == "gpu":
        return resize_img(images, img_dims), labels

    if mode == "train":  # RandAug only during finetune or scratch train and if wanted
        fun = simclr_augment

    if resize:
        images = resize_img(images, img_dims)

    in_shape = tf.shape(images)
    # TODO: Currently the pretrain cannot work. 'Fun' will be undefined
    #  and if the augs are on the GPU, then pretrain is unreachable.
    if mode == "pretrain":
        def rotate_image(arg):
            x = tf.image.rot90(arg[0], k=arg[1])
            return x

        # Apply the set of base augmentations
        images1 = fun(images)
        images2 = fun(images)
        # Rotate all images randomly
        labels = tf.random.uniform([int(2 * in_shape[0])], maxval=4, dtype=tf.int32)
        images = tf.vectorized_map(rotate_image, [tf.concat([images1, images2], 0), labels])

        [images1, images2] = tf.split(images, num_or_size_splits=2, axis=0)
        images = tf.concat([images1, images2], -1)
        labels = tf.one_hot(labels, 4)

    else:
        images = fun(images)

    return images, labels


def build_lbl_pred_png(lbls, probs):
    # Build a nice matplotlib figure
    f, ax = plt.subplots(2, 1, figsize=(10, 5))
    ax[0].imshow(tf.transpose(lbls), cmap="gray")
    ax[0].text(0, 1.1, "Original Coefficients", transform=ax[0].transAxes)
    # ax[1].imshow(tf.transpose(preds), cmap="gray")
    # ax[1].text(0, 1.1, "Predictions", transform=ax[1].transAxes)
    ax[1].imshow(tf.transpose(probs), cmap="gray")
    ax[1].text(0, 1.1, "Predicted Coefficients", transform=ax[1].transAxes)
    for a in ax:
        a.axis(False)
    f.tight_layout()

    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close(f)
    buf.seek(0)
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image


def without_keys(d, keys):
    return {x: d[x] for x in d if x not in keys}


def patches_to_images(arg, arg_shape, box_size):
    patches_ori = tf.reshape(arg, arg_shape)
    patches_rec = tf.nn.depth_to_space(patches_ori, box_size)
    return patches_rec


def images_to_patches(arg, bs, size, channels, filter_size):
    patches = tf.nn.space_to_depth(arg, int(filter_size))
    patch_shape = tf.shape(patches)
    num_patches = size / filter_size
    new_shape = [int(float(bs) * float(num_patches) * float(num_patches)),
                 int(filter_size), int(filter_size), int(channels)]
    patches = tf.reshape(patches, new_shape)

    return patches, patch_shape


def get_random_filter_sizes(size, odd=False):
    start_size = size // 10
    if start_size == 0:
        start_size = 1
    possible_filter_sizes = tf.range(start_size, size // 2, 1, dtype=tf.int32)
    good_sizes = tf.where(tf.math.floormod(size, possible_filter_sizes) == 0)
    valid_kernel_sizes = tf.gather(possible_filter_sizes,
                                   good_sizes)

    rnd_kernel_size_idx = tf.random.uniform([1],
                                            minval=0,
                                            maxval=tf.shape(valid_kernel_sizes)[0],
                                            dtype=tf.int32, seed=1)

    filter_size = tf.gather(valid_kernel_sizes, rnd_kernel_size_idx)[0]
    if odd:
        filter_size = 2 * (filter_size // 2) + 1
    return filter_size[0].numpy()


def get_random_idx(num_patches, batch_size, shape=False, even=False):
    if shape:
        idx_shape = shape
    else:
        idx_shape = tf.random.uniform(shape=[1], minval=batch_size,
                                      maxval=num_patches // 5, dtype=tf.int32)
        if even:
            idx_shape = 2 * (idx_shape // 2)
    idxs = tf.random.uniform(shape=idx_shape, minval=0,
                             maxval=num_patches, dtype=tf.int32)
    return idxs


def random_grey(patches, num_patches, batch_size, channel):
    idxs = get_random_idx(num_patches, batch_size)
    patch_gathered = tf.gather(patches, idxs)
    patch_bw_single_dim = tf.expand_dims(tf.reduce_mean(patch_gathered, axis=-1), -1)
    patch_bw = tf.repeat(patch_bw_single_dim,
                         channel, 3)
    patches = tf.tensor_scatter_nd_update(patches, tf.expand_dims(idxs, -1), patch_bw)
    return patches


def random_drop_replace(patches, num_patches, batch_size):
    idxs = get_random_idx(num_patches, batch_size, even=True)
    idxs_replace, idxs_noise = tf.split(idxs, 2)

    patch_gathered_noise = tf.gather(patches, idxs_noise)
    idxs_replace_new = get_random_idx(num_patches, batch_size, tf.shape(idxs_replace))
    patch_replace = tf.gather(patches, idxs_replace_new)

    patch_noise = tfd.Uniform().sample(tf.shape(patch_gathered_noise))
    patch = tf.concat([patch_replace, patch_noise], 0)
    patches = tf.tensor_scatter_nd_update(patches, tf.expand_dims(idxs, -1), patch)
    return patches


def gaussian_kernel(size: int,
                    std: float,
                    ):
    """
    Makes 2D gaussian Kernel for convolution.
    """
    d = tfd.Normal(loc=0, scale=std)

    prob_range = tf.range(start=0, limit=size, dtype=tf.float32) - float(size) // 2
    vals = tf.expand_dims(d.prob(prob_range), -1)
    gauss_kernel = tf.matmul(vals, vals,
                             transpose_b=True)

    return gauss_kernel / tf.reduce_sum(gauss_kernel)


def random_blur(patches, num_patches, batch_size, channels, patch_size, max_sigma=5):
    idxs = get_random_idx(num_patches, batch_size)
    patch_gathered = tf.gather(patches, idxs)
    patch_gathered_shape = tf.shape(patch_gathered)
    size = get_random_filter_sizes(patch_size, odd=True)
    rnd_sigma_size = tf.random.uniform([1],
                                       minval=.1,
                                       maxval=max_sigma + .1,
                                       dtype=tf.float32, seed=1)

    gauss_kernel = gaussian_kernel(size=size,
                                   std=rnd_sigma_size ** 2)

    # Expand dimensions of `gauss_kernel` for `tf.nn.conv2d` signature
    # print(patch_size, size, gauss_kernel.shape)
    gauss_kernel = tf.expand_dims(tf.expand_dims(gauss_kernel[:, :], -1), -1)
    gauss_kernel = tf.broadcast_to(gauss_kernel,
                                   (size, size, int(channels), 1))

    # Convolve
    patch_blurred = tf.reshape(patch_gathered, [patch_gathered_shape[0], patch_size, patch_size, int(channels)])
    pad_size = size // 2
    patch_blurred = tf.pad(patch_blurred,
                           [[0, 0], [pad_size, pad_size], [pad_size, pad_size], [0, 0]],
                           "REFLECT")

    patch_blurred = tf.nn.depthwise_conv2d(patch_blurred,
                                           gauss_kernel,
                                           strides=(1, 1, 1, 1),
                                           padding="VALID")

    patches = tf.tensor_scatter_nd_update(patches, tf.expand_dims(idxs, -1), patch_blurred)
    return patches


def distort_images(images):
    # Special pretrain augmentation as in section 3 of:
    # Atito, S., Awais, M. & Kittler, J.
    # SiT: Self-supervised vIsion Transformer. (2021)

    # random drop and random replace, colour distortions,
    # recolouring, blurring, grey-scale

    # As for the colour transformations, it involves basic adjustments
    # of colour levels in an image including colour distortions,
    # blurring, and converting to grey-scale. Colour distortions are
    # applied to the whole image to enable the network to recognise similar
    # images invariant to their colours. Unlike colour distortions,
    # blurring and conversion to grey-scale transformations are applied
    # to the local neigh- bourhood of arbitrary patches of the image rather
    # than the full image to enable the network to learn the texture and
    # colour transformations from the surrounding pixels. Blurring is
    # performed by applying a Gaussian filter to the selected patches and
    # converting to grey-scale is performed by a linear transformation of
    # the coloured (RGB) patches.
    undistorted_images_list = tf.split(
        images, num_or_size_splits=2, axis=-1)
    sh = tf.shape(undistorted_images_list[0])
    undistorted_images = tf.concat(undistorted_images_list, 0)
    batch_size, height, width, channels = 2 * sh[0], sh[1], sh[2], sh[3]
    filter_size = get_random_filter_sizes(height)

    distorted_images, distorted_images_shape = images_to_patches(undistorted_images, batch_size,
                                                                 height, channels, filter_size)

    num_patches = tf.shape(distorted_images)[0]
    distorted_images = random_grey(distorted_images, num_patches,
                                   batch_size, channels)

    distorted_images = random_blur(distorted_images, num_patches,
                                   batch_size, channels, filter_size)

    distorted_images = random_drop_replace(distorted_images, num_patches,
                                           batch_size)

    distorted_images = patches_to_images(distorted_images, distorted_images_shape, filter_size)

    # Back together
    distorted_images_list = tf.split(
        distorted_images, num_or_size_splits=2, axis=0)
    distorted_images = tf.concat(distorted_images_list, -1)
    return tf.concat([images, distorted_images], -1)


def annotate_heatmap(im, data=None, valfmt="{x:.2f}",
                     textcolors=("black", "white"),
                     threshold=None, **textkw):
    """
    A function to annotate a heatmap.

    Parameters
    ----------
    im
        The AxesImage to be labeled.
    data
        Data used to annotate.  If None, the image's data is used.  Optional.
    valfmt
        The format of the annotations inside the heatmap.  This should either
        use the string format method, e.g. "$ {x:.2f}", or be a
        `matplotlib.ticker.Formatter`.  Optional.
    textcolors
        A pair of colors.  The first is used for values below a threshold,
        the second for those above.  Optional.
    threshold
        Value in data units according to which the colors from textcolors are
        applied.  If None (the default) uses the middle of the colormap as
        separation.  Optional.
    **kwargs
        All other arguments are forwarded to each call to `text` used to create
        the text labels.
    """

    if not isinstance(data, (list, np.ndarray)):
        data = im.get_array()

    # Normalize the threshold to the images color range.
    if threshold is not None:
        threshold = im.norm(threshold)
    else:
        threshold = im.norm(data.max()) / 2.

    # Set default alignment to center, but allow it to be
    # overwritten by textkw.
    kw = dict(horizontalalignment="center",
              verticalalignment="center")
    kw.update(textkw)

    # Get the formatter in case a string is supplied
    if isinstance(valfmt, str):
        valfmt = matplotlib.ticker.StrMethodFormatter(valfmt)

    # Loop over the data and create a `Text` for each "pixel".
    # Change the text's color depending on the data.
    texts = []
    for i in range(data.shape[0]):
        for j in range(data.shape[1]):
            kw.update(color=textcolors[int(im.norm(data[i, j]) > threshold)])
            text = im.axes.text(j, i, valfmt(data[i, j], None), **kw)
            texts.append(text)


def heatmap(data, row_labels, col_labels, ax=None, **kwargs):
    """
    Create a heatmap from a numpy array and two lists of labels.

    Parameters
    ----------
    data
        A 2D numpy array of shape (N, M).
    row_labels
        A list or array of length N with the labels for the rows.
    col_labels
        A list or array of length M with the labels for the columns.
    ax
        A `matplotlib.axes.Axes` instance to which the heatmap is plotted.  If
        not provided, use current axes or create a new one.  Optional.
    **kwargs
        All other arguments are forwarded to `imshow`.
    """

    if not ax:
        ax = plt.gca()

    # Plot the heatmap
    im = ax.imshow(data, **kwargs)

    # We want to show all ticks...
    ax.set_xticks(np.arange(data.shape[1]))
    ax.set_yticks(np.arange(data.shape[0]))
    # ... and label them with the respective list entries.
    ax.set_xticklabels(col_labels)
    ax.set_yticklabels(row_labels)

    # Let the horizontal axes labeling appear on top.
    ax.tick_params(top=True, bottom=False,
                   labeltop=True, labelbottom=False)

    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=-30, ha="right",
             rotation_mode="anchor")

    # Turn spines off and create white grid.
    ax.spines[:].set_visible(False)

    ax.set_xticks(np.arange(data.shape[1] + 1) - .5, minor=True)
    ax.set_yticks(np.arange(data.shape[0] + 1) - .5, minor=True)
    ax.grid(which="minor", color="w", linestyle='-', linewidth=3)
    ax.tick_params(which="minor", bottom=False, left=False)

    return im


def get_task_heatmap(args, channel_wise_loss, channel_wise_logits, channel_wise_labels):
    num_coefs = int(3 + args.degree) if args.minimize_power_spectrum else int(2 + np.square(args.degree + 1))
    fig, ax = plt.subplots(figsize=(np.round(num_coefs / 1.25).astype(int), 4))
    cw_loss = tf.squeeze(channel_wise_loss)
    cw_logits = tf.squeeze(channel_wise_logits)
    cw_labels = tf.squeeze(channel_wise_labels)

    mult_weights = tf.cast(tf.concat([[180, 360, args.mean_radius], tf.ones(int(num_coefs - 3))], 0), cw_loss.dtype)
    add_weights = tf.cast(tf.concat([[90], tf.zeros(int(num_coefs - 1))], 0), cw_loss.dtype)
    cw_loss_real_units = tf.math.add(tf.math.multiply(cw_loss, mult_weights), add_weights)

    stacked_data = tf.stack([tf.squeeze(cw_logits), tf.squeeze(cw_labels),
                             tf.squeeze(cw_loss), tf.squeeze(cw_loss_real_units)]
                            )
    max_size = tf.reduce_max(stacked_data)
    im = heatmap(stacked_data, ["Preds", "Labels", "MAE", "Real dev."],
                 ["lat", "lon", "size"] + ["{}".format(x + 1) for x in range(int(num_coefs - 3))],
                 ax=ax, cmap="binary", vmin=max_size, vmax=max_size)
    annotate_heatmap(im, valfmt="{x:.2f}")
    ax.grid(False)
    fig.tight_layout()
    # Save the plot to a PNG in memory.
    buf = io.BytesIO()
    plt.savefig(buf, format='png')
    plt.close(fig)
    buf.seek(0)
    # Convert PNG buffer to TF image
    image = tf.image.decode_png(buf.getvalue(), channels=4)
    # Add the batch dimension
    image = tf.expand_dims(image, 0)
    return image


class ScatmanGenerator(object):
    def __init__(self,
                 bs,
                 wavelength,
                 angle,
                 resolution,
                 delta,
                 beta,
                 mean_radius,
                 degree=8,
                 gpus=0,
                 exp_weight=.1,
                 use_cpu=True,
                 power_spectrum=True,
                 **kwargs):
        """
        This class implements a Generator with infinite
        length for use with the PyScatman module.

        :param bs: The batch size
        :param wavelength: The wavelength of the irradiation
        :param angle: The maximal scattering angle
        :param resolution: Resolution in px of the output images
        :param delta: Real part of the refractive index (X-Ray Nomenclature: n'= 1 - delta)
        :param beta: Imaginary part of the refractive index
        :param mean_radius: Mean radius of the target particles in units of the wavelength
        :param degree: The max degree (l value) up to which spherical harmonics should be calculated
        :param gpus: Integer - or list of integers - specifying the GPUs to use
                     Ignored when use_cpu is True
        :param exp_weight: The weighting factor for constructing the exponential weights.
                           This value is by how much the 10th order should weigh
                           -> in percent points, so 1 = 100%
        :param use_cpu: Boolean if the CPU should be used
        :param power_spectrum: Return the power spectrum instead of the full harmonic spectrum
        :param kwargs: Additional keyword arguments
        """
        super(ScatmanGenerator, self).__init__(**kwargs)
        self.n = 0
        self.bs = bs
        self.wavelength = wavelength
        self.angle = angle
        self.resolution = np.round(resolution / np.cos(np.pi / 4)).astype(int)
        self.delta = delta
        self.beta = beta
        self.mean_radius = mean_radius
        self.degree = degree
        self.use_cpu = use_cpu
        self.exp_weight = exp_weight
        self.power_spectrum = power_spectrum
        if self.use_cpu:
            scatman.use_cpu()
        else:
            scatman.use_gpu()
            scatman.set_gpu_ids(gpus)

        # Setting up the experiment
        scatman.set_experiment(wavelength=self.wavelength,
                               angle=self.angle,
                               resolution=self.resolution)
        # Initializing
        scatman.init()
        # defining the detector
        self.detector = scatman.Detectors.MSFT()
        # self.detector = scatman.Detectors.Ideal(mask_radius=int(.15 * self.resolution))
        # Initializing the random number generator
        self.rng = np.random.default_rng(seed=None)
        # scatman.info()

    def __iter__(self):
        return self

    # Python 3 compatibility
    def __next__(self):
        return self.next()

    @staticmethod
    def get_degree_indices(degree):
        """
        This return the indices for all l, m pairs that belong to a fixed l (the degree) value
        :param degree: the value for which the l, m pair indices should be returned
        :return: the indices
        """
        assert degree >= 0
        return np.arange(int(degree ** 2), int((1 + degree) ** 2)).astype(int)

    def get_power_spectrum(self, coeffs):
        """
        This returns the power spectrum for a given set of l, m coefficients
        :param coeffs: the l, m pairs, passed as a 1-d list
        :return: the power spectrum
        """
        ps_len = int(np.sqrt(len(coeffs)))
        ps = []
        for degree in range(ps_len):
            d_ind = self.get_degree_indices(degree)
            c_ = coeffs[d_ind]
            ps.append(np.sum(np.square(c_)))

        return np.array(ps)

    def next(self):
        # Orientation is uniformly distributed
        orientation_params = self.rng.uniform(0, 1, [self.bs, 2])
        # Size is log-normally distributed according to the Hagena scaling law
        # The sigma parameter of 0.25 is manually estimated based on the size distribution
        # in Phys. Rev. Lett. 121, 255301.
        size_params = self.rng.lognormal(np.log(self.mean_radius), .25, [self.bs, 1])
        # The coefficients cover all l, m pairs with l up to 8. lower l values are
        # exponentially more important than higher ones
        num_l_vals = int((self.degree + 1) ** 2 - 1)

        exp_weights_step = []
        exp_weights = np.exp(np.log(self.exp_weight) / 10 * np.arange(self.degree + 1))
        for ll in range(self.degree + 1):
            exp_weights_step.extend([exp_weights[ll]] * len(self.get_degree_indices(ll)))

        coefficient_unweighted = self.rng.uniform(0, 1, [self.bs, num_l_vals])
        spectral_params = coefficient_unweighted * np.array(exp_weights_step)[1:]
        shapes = [scatman.Shapes.SphericalHarmonics(
            a=s,
            # For example:  l up to 7 -> 64 parameter with exponentially decreasing importance. First val is always 1.
            coefficients=np.insert(c, 0, 1),
            latitude=o[0] * 180. - 90,  # Between -90° and 90°
            longitude=o[1] * 360.,  # Between 0° and 360°
            delta=self.delta,
            beta=self.beta) for o, s, c in zip(orientation_params,
                                               size_params,
                                               spectral_params)]
        # Get the patterns
        patterns = self.detector.acquire(shapes)
        if self.power_spectrum:
            # Return everything ... ignore the power spectrum term for l=0... it is always 1
            spectral_params = [self.get_power_spectrum(np.insert(c, 0, 1))[1:] for c in spectral_params]

        # Stack the parameter
        params = np.hstack([orientation_params, size_params / self.mean_radius, spectral_params])
        patterns_num = [np.expand_dims(x, -1) for x in patterns]
        self.n += 1
        return patterns_num, params

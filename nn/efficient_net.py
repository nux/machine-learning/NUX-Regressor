# Copyright 2019 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
"""EfficientNet models for Keras.

Reference:
  - [EfficientNet: Rethinking Model Scaling for Convolutional Neural Networks](
      https://arxiv.org/abs/1905.11946) (ICML 2019)
"""

import tensorflow as tf
from abc import ABC

tfk = tf.keras
tfkl = tfk.layers

CONV_KERNEL_INITIALIZER = {
    "class_name": "VarianceScaling",
    "config": {
        "scale": 2.0,
        "mode": "fan_out",
        "distribution": "truncated_normal"
    }
}

DENSE_KERNEL_INITIALIZER = {
    "class_name": "VarianceScaling",
    "config": {
        "scale": 1. / 3.,
        "mode": "fan_out",
        "distribution": "uniform"
    }
}


class Head(tfk.Model, ABC):
    def __init__(self, out_units, drop_connect_rate,
                 activation="swish", dropout_rate=0.2,
                 name="RegressionHead",
                 **kwargs):
        super(Head, self).__init__(name=name, **kwargs)

        # Build the classifier / regressor stack
        self.gl_bn = tfkl.GlobalAveragePooling2D()
        self.resh = tfkl.Reshape([-1])
        self.bn1 = tfkl.BatchNormalization(name=name + "_bn_0")
        self.dr1 = tfkl.Dropout(drop_connect_rate, name=name + "_dropout_0")
        self.dense1 = tfkl.Dense(512,
                                 kernel_initializer=DENSE_KERNEL_INITIALIZER,
                                 name=name + "_dense")
        self.act = tfkl.Activation(activation, name=name + "_activation")
        self.bn2 = tfkl.BatchNormalization(name=name + "_bn_1")
        self.dr2 = tfkl.Dropout(dropout_rate, name=name + "_dropout_1")
        self.out = tfkl.Dense(out_units, kernel_initializer=DENSE_KERNEL_INITIALIZER,
                              name=name + "_dense_out")

    def call(self, inputs, training=None):
        inputs = self.gl_bn(inputs, training=training)
        inputs = self.resh(inputs, training=training)
        inputs = self.bn1(inputs, training=training)
        inputs = self.dr1(inputs, training=training)
        inputs = self.dense1(inputs, training=training)
        inputs = self.act(inputs, training=training)
        inputs = self.bn2(inputs, training=training)
        inputs = self.dr2(inputs, training=training)

        return self.out(inputs, training=training)


class EfficientNet(tfk.Model, ABC):
    def __init__(self, encoder, drop_connect_rate, output_dim=10,
                 activation="swish", dropout_rate=0.2,
                 name="EfficientNet",
                 **kwargs):
        super(EfficientNet, self).__init__(name=name, **kwargs)

        self.encoder = encoder
        self.output_dim = output_dim

        shared_head_args = {"drop_connect_rate": drop_connect_rate,
                            "activation": activation,
                            "dropout_rate": dropout_rate}

        self.reg_head = Head(self.output_dim, **shared_head_args, name="regression_head")
        # This linear activation is needed in case of mixed precision training,
        # it then acts as a casting layer
        self.out = tfkl.Activation("linear", dtype="float32", name="Casting")

    # noinspection PyCallingNonCallable
    def call(self, inputs, training=None):
        top_features = self.encoder(inputs, training=training)
        reg_head = self.reg_head(top_features, training=training)
        return self.out(reg_head), inputs


def efficientnetb0(output_dim, input_shape, **kwargs):
    encoder = tfk.applications.efficientnet.EfficientNetB0(
        include_top=False, weights=None,
        input_shape=input_shape, pooling=None,
        **kwargs
    )
    return EfficientNet(
        output_dim=output_dim,
        encoder=encoder,
        drop_connect_rate=0.2,
        name="efficient_net_b0",
        **kwargs)


def efficientnetb1(output_dim, input_shape, **kwargs):
    encoder = tfk.applications.efficientnet.EfficientNetB1(
        include_top=False, weights=None,
        input_shape=input_shape, pooling=None,
        **kwargs
    )
    return EfficientNet(
        output_dim=output_dim,
        encoder=encoder,
        drop_connect_rate=0.2,
        name="efficient_net_b1",
        **kwargs)


def efficientnetb2(output_dim, input_shape, **kwargs):
    encoder = tfk.applications.efficientnet.EfficientNetB2(
        include_top=False, weights=None,
        input_shape=input_shape, pooling=None,
        **kwargs
    )
    return EfficientNet(
        output_dim=output_dim,
        encoder=encoder,
        drop_connect_rate=0.3,
        name="efficient_net_b2",
        **kwargs)


def efficientnetb3(output_dim, input_shape, **kwargs):
    encoder = tfk.applications.efficientnet.EfficientNetB3(
        include_top=False, weights=None,
        input_shape=input_shape, pooling=None,
        **kwargs
    )
    return EfficientNet(
        output_dim=output_dim,
        encoder=encoder,
        drop_connect_rate=0.3,
        name="efficient_net_b3",
        **kwargs)


def efficientnetb4(output_dim, input_shape, **kwargs):
    encoder = tfk.applications.efficientnet.EfficientNetB4(
        include_top=False, weights=None,
        input_shape=input_shape, pooling=None,
        **kwargs
    )
    return EfficientNet(
        output_dim=output_dim,
        encoder=encoder,
        drop_connect_rate=0.4,
        name="efficient_net_b4",
        **kwargs)


def efficientnetb5(output_dim, input_shape, **kwargs):
    encoder = tfk.applications.efficientnet.EfficientNetB5(
        include_top=False, weights=None,
        input_shape=input_shape, pooling=None,
        **kwargs
    )
    return EfficientNet(
        output_dim=output_dim,
        encoder=encoder,
        drop_connect_rate=0.4,
        name="efficient_net_b5",
        **kwargs)


def efficientnetb6(output_dim, input_shape, **kwargs):
    encoder = tfk.applications.efficientnet.EfficientNetB6(
        include_top=False, weights=None,
        input_shape=input_shape, pooling=None,
        **kwargs
    )
    return EfficientNet(
        output_dim=output_dim,
        encoder=encoder,
        drop_connect_rate=0.5,
        name="efficient_net_b6",
        **kwargs)


def efficientnetb7(output_dim, input_shape, **kwargs):
    encoder = tfk.applications.efficientnet.EfficientNetB7(
        include_top=False, weights=None,
        input_shape=input_shape, pooling=None,
        **kwargs
    )
    return EfficientNet(
        output_dim=output_dim,
        encoder=encoder,
        drop_connect_rate=0.5,
        name="efficient_net_b7",
        **kwargs)

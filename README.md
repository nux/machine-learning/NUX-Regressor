### TF 2 implementation of a Self-Supervised Class-Attention Cross-Covariance Visual Transformer ###

The TF2 implementations of the following papers can be found in this repo:

1. Dosovitskiy, A. et al. An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale. (2020).
2. Shazeer, N., Lan, Z., Cheng, Y., Ding, N. & Hou, L. Talking-Heads Attention. (2020).
3. Touvron, H., Cord, M., Sablayrolles, A., Synnaeve, G. & Jégou, H. Going deeper with Image Transformers. (2021).
4. Atito, S., Awais, M. & Kittler, J. SiT: Self-supervised vIsion Transformer. (2021).
5. El-Nouby, A. et al. XCiT: Cross-Covariance Image Transformers. (2021).
6. Xiao, T. et al. Early Convolutions Help Transformers See Better. (2021).

These papers essentially adapt the attention mechanism, originally proposed by [Ashish Vaswani et al.](https://arxiv.org/abs/1706.03762), to the domain of images.
Ref., [1] is the principal Visual Transformer by Google, which was improved by Facebook in [3] using TalkingHeads attention for self-attention [2] and their proposed class-attention mechanism and LayerScale normalization layer.
Further improvement on scalability was done in [5], using tranposed cross-covariance attention that reduces the quadratic scaling with token length to a linear scaling, while maintaining the strong performance of other ViT arcs.
In addition, this repository contains a fully working self-supervise routine based on [4], which incorporate elements from [contrastive learning](http://arxiv.org/abs/2002.05709), representation learning and [uncertainty estimation in multi-task learning scenarios](https://arxiv.org/abs/1705.07115v3).
Then, [6] showed that using a Concolutional Deep Root, instead of the standard patchify root of ViT, greatly improves training stablity while boosting accuracy.

#### Furthermore, this implementation has:
* Full Multi-GPU support
* Augmentation using [RandAugment](https://arxiv.org/abs/1909.13719)
* [Sharpness-Aware Minimization for Efficiently Improving Generalization](https://arxiv.org/abs/2010.01412)
* [Decoupled Weight Decay Regularization](https://arxiv.org/abs/1711.05101)
* Linear warm-up [cosine learning rate scheduler](https://arxiv.org/abs/1608.03983) with the [AdamW](https://arxiv.org/abs/1711.05101) optimizer
* [Stochastic Depth](https://arxiv.org/abs/1603.09382) instead of residual connections
* The [GeLU activation function](https://arxiv.org/abs/1606.08415) for hidden layers
* Mixed Precicion can be activated by setting ```--fp16 true```

#### Current supported flags:
```
  -h, --help            show this help message and exit
  --model [{xcit_tst,cait_xxs24,cait_xs36,cait_s24,cait_m24,cait_s48,cait_s36,xcit_s12,xcit_s24,xcit_m24}]
                        The model name. One of: cait_xxs24, cait_xs36, cait_s24, cait_m24, cait_s48, cait_s36, xcit_s12, xcit_s24,, xcit_m24
  --dataset [DATASET]   String that is passed to Squirrel. See sq.list_builders() for avail. datasets.
  --split_list SPLIT_LIST [SPLIT_LIST ...]
                        The 'split' argument for squirrel. A python 'list' is expected. Example: ['test', 'train'] can be passed as: python main.py --split_list test train
  --epochs [EPOCHS]     The number of epochs to run
  --save_every_n_epoch [SAVE_EVERY_N_EPOCH]
                        Save model weights every 'n' epochs
  --test_every_n_epoch [TEST_EVERY_N_EPOCH]
                        Evaluate model every 'n' epochs
  --image_size [image_size]
                        Image size to which the input will be resized
  --image_channels [image_channels]
                        Image channel to which the input will be expanded/reduced
  --log_steps [LOG_STEPS]
                        Write out logs every 'n' steps
  --batch_size [BATCH_SIZE]
                        The size of batch per gpu.
  --clr_temperature [CLR_TEMPERATURE]
                        The temperature parameter for the contrastive task during pre-training
  --optimizer [{sgd,adam,adamw}]
                        The used optimizer. Choices are either 'sgd', 'adam', 'adamw'
  --lr [LR]             Learning rate. Leave zero for batch-size dependent estimation of the lr'
  --l2 [L2]             L2 regularization term. Sometimes called 'weight decay'
  --deep_root [{True,False}]
                        Use a deep root instead of the standard ViT patchify stem
  --sam [{True,False}]  Use sharpness-aware minimization
  --sam_rho SAM_RHO     Rho for the sharpness-aware minimization
  --use_fourier_embedding [{True,False}]
                        Use a Fourier Embedding as in the original Vaswani Paper
  --patch_size [{16,8}]
                        The input patch size
  --base_model_dir [BASE_MODEL_DIR]
                        This argument is only used in the fine-tune routine,and provides a path to a base model to use. Thisbase model has to be pre-trained.
  --base_model_epoch [BASE_MODEL_EPOCH]
                        This argument is only used in the fine-tune routine,The epoch to load. Leave this empty for the latest model.
  --fp16 [{True,False}]
                        Use mixed precision or not. Set to True if your NVidia GPU has compute capability > 7.0
  --gpus [GPUS]         Number of GPUs to be used. Can be index-based '0,1,2' or 'all' for all available
  --work_dir [WORK_DIR]
                        The work directory. Missing directories will be created
```

### This repo has also a second branch "conv", where the following can be found:

Tensorflow > 2.3 implementation of:

* Squeeze-and-Excitation Networks | Jie Hu, et al. arXiv:1709.01507 [cs.CV]

* ResNeSt: Split-Attention Networks | Zhang, H. et al. (2020) arXiv:2004.08955 [cs.CV]

#### Using:
* The SENet is using the [ResNext](https://arxiv.org/abs/1611.05431) architecture as backend
* Full contrastive pre-training routine is available:
  1) [Chen, T., et al.  // A Simple Framework for Contrastive Learning of Visual Representations](http://arxiv.org/abs/2002.05709)
  2) [Chen, T., et al.  // Big Self-Supervised Models are Strong Semi-Supervised Learners](http://arxiv.org/abs/2006.10029)
"""
### TF2 implementation of a Class-Attention Cross-Covariance Visual Transformer ###

The TF2 implementations of the following papers:

1. Dosovitskiy, A. et al. An Image is Worth 16x16 Words: Transformers for Image Recognition at Scale. (2020).
2. Shazeer, N., Lan, Z., Cheng, Y., Ding, N. & Hou, L. Talking-Heads Attention. (2020).
3. Touvron, H., Cord, M., Sablayrolles, A., Synnaeve, G. & Jégou, H. Going deeper with Image Transformers. (2021).
4. Atito, S., Awais, M. & Kittler, J. SiT: Self-supervised vIsion Transformer. (2021).
5. El-Nouby, A. et al. XCiT: Cross-Covariance Image Transformers. (2021).
6. Xiao, T. et al. Early Convolutions Help Transformers See Better. (2021).

Author: jzimmermann@phys.ethz.ch

    For fine-tuning:
    From: Chen, T. et al. A Simple Framework for Contrastive Learning of Visual Representations,
    and Chen, T. et al. Big Self-Supervised Models are Strong Semi-Supervised Learners:

    A deeper projection head not only improves the representation quality
    measured by linear evaluation, but also improves semi-supervised performance
    when fine-tuning from a middle layer of the projection head.

    Fine-tuning is a common way to adapt the task-agnostically pretrained network
    for a specific task. In SimCLR [1], the MLP projection head g(·) is discarded
    entirely after pretraining, while only the ResNet encoder f(·) is used during
    the fine-tuning. Instead of throwing it all away, we propose to incorporate part
    of the MLP projection head into the base encoder during the fine-tuning. In
    other words, we fine-tune the model from a middle layer of the projection head,
    instead of the input layer of the projection head as in SimCLR. Note that
    fine-tuning from the first layer of the MLP head is the same as adding an
    fully-connected layer to the base network and removing an fully-connected
    layer from the head, and the impact of this extra layer is contingent on the
    amount of labeled examples during fine-tuning (as shown in our experiments).

    For fine-tuning, by default we fine-tune from the first layer of the projection
    head for 1%/10% of labeled examples, but from the input of the projection head
    when 100% labels are present. We use global batch normalization, but we remove
    weight decay, learning rate warmup, and use a much smaller learning rate,
    i.e. 0.16 (= 0.005 × sqrt(BatchSize)) for standard ResNets [25], and
    0.064 (= 0.002 × sqrt(BatchSize)) for larger ResNets variants (with width
    multiplier larger than 1 and/or SK [28]). A batch size of 1024 is used.
    Similar to [1], we fine-tune for 60 epochs with 1% of labels, and 30 epochs
    with 10% of labels, as well as full ImageNet labels.
"""
import os
import nn
import json
import socket
import logging
from tqdm import tqdm
import tensorflow as tf
from numpy import savez
from itertools import takewhile

tfk = tf.keras
tfkl = tfk.layers


# ============================================
# Optimisation Flags
# see for example: https://github.com/NVIDIA/DeepLearningExamples/issues/57
# ============================================

# os.environ['CUDA_CACHE_DISABLE'] = '0'
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
# os.environ['TF_GPU_THREAD_MODE'] = 'gpu_private'
# os.environ['TF_GPU_THREAD_COUNT'] = '1'
# os.environ['TF_USE_CUDNN_BATCHNORM_SPATIAL_PERSISTENT'] = '1'
#
# os.environ['TF_ADJUST_HUE_FUSED'] = '1'
# os.environ['TF_ADJUST_SATURATION_FUSED'] = '1'
# os.environ['TF_ENABLE_WINOGRAD_NONFUSED'] = '1'
#
# os.environ['TF_SYNC_ON_FINISH'] = '0'
# os.environ['TF_AUTOTUNE_THRESHOLD'] = '2'
# os.environ['TF_DISABLE_NVTX_RANGES'] = '1'
#
# if socket.gethostname() == "nux-noether":
#     # os.environ["TF_GPU_ALLOCATOR"] = "cuda_malloc_async"
#     os.environ["XLA_FLAGS"] = "--xla_gpu_cuda_data_dir=/usr/local/cuda-11/"
# os.environ["TF_XLA_FLAGS"] = "--tf_xla_auto_jit=2 " \
#                              "--tf_xla_cpu_global_jit " \
#                              "--tf_xla_enable_xla_devices"


def scratch(args):
    @tf.function
    def train_step(dist_input):
        def forward_pass():
            with tf.GradientTape() as tape:
                logits, iimgs = model(image_batch, training=True)
                task_loss = nn.get_loss(args.loss_function)(labels=label_batch, preds=logits, args=args)

                # If Adamw is used, this will always be zero
                reg_loss_unscaled = tf.reduce_sum([tf.cast(x, tf.float32) for x in model.losses])
                regularization_loss = tf.nn.scale_regularization_loss(reg_loss_unscaled)

                total_loss = task_loss + regularization_loss
                if args.fp16:
                    scaled_loss = optimizer.get_scaled_loss(total_loss)
                else:
                    scaled_loss = total_loss

            grads = tape.gradient(scaled_loss, model.trainable_variables)
            if args.fp16:
                grads = optimizer.get_unscaled_gradients(grads)

            return_args = [total_loss, task_loss, regularization_loss, label_batch, logits, iimgs]
            return grads, return_args

        image_batch, label_batch = dist_input[0], dist_input[1]
        gradients, ret_arr = forward_pass()

        if args.sam:
            # Sharpness-aware minimization:
            # Foret, P., Kleiner, A., Mobahi, H. & Neyshabur, B.
            # Sharpness-Aware Minimization for Efficiently Improving Generalization. (2020).
            e_ws = []
            grad_norm = tf.linalg.global_norm(gradients)
            for i_var in range(len(model.trainable_variables)):
                e_w = gradients[i_var] * args.sam_rho / (grad_norm + 1e-8)
                model.trainable_variables[i_var].assign_add(e_w)
                e_ws.append(e_w)

            gradients, ret_arr = forward_pass()

            for i_var in range(len(model.trainable_variables)):
                model.trainable_variables[i_var].assign_add(-e_ws[i_var])

        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        return ret_arr

    @tf.function
    def eval_step(dist_input):
        image_batch, label_batch = dist_input[0], dist_input[1]
        logits, iimgs = model(image_batch, training=False)
        task_loss = nn.get_loss(args.loss_function)(labels=label_batch, preds=logits, args=args)

        ret_arr = [task_loss, label_batch, logits, iimgs]
        return ret_arr

    # noinspection PyProtectedMember
    @tf.function
    def distributed_train_step(dist_inputs):
        _arr = strategy.run(train_step, args=(dist_inputs,))
        loss, task_loss, reg_loss, lbl, logits, imgs = _arr

        if strategy.num_replicas_in_sync > 1:
            lbl = strategy.gather(lbl, 0)
            logits = strategy.gather(logits, 0)

        lbl = tf.cast(tf.squeeze(lbl), tf.float32)
        logits = tf.cast(tf.squeeze(logits), tf.float32)
        loss = strategy.reduce("SUM", loss, axis=None)
        task_loss = strategy.reduce("SUM", task_loss, axis=None)
        reg_loss = strategy.reduce("SUM", reg_loss, axis=None)

        for key_, val_ in zip(metrics._fields, metrics):
            if key_ == "total_loss__train":
                val_.update_state(values=loss)
            if key_ == "mape__train":
                val_.update_state(lbl, logits)
            if key_ == "task_loss__train":
                val_.update_state(values=task_loss)
            if key_ == "regularization_loss__train":
                val_.update_state(values=reg_loss)
            if key_ == "channel_wise_loss__train":
                mae_channel_wise = tf.math.reduce_mean(tf.math.abs(lbl - logits), axis=0)
                val_.update_state(values=mae_channel_wise)
            if key_ == "channel_wise_logits__train":
                val_.update_state(values=tf.math.reduce_mean(logits, axis=0))
            if key_ == "channel_wise_labels__train":
                val_.update_state(values=tf.math.reduce_mean(lbl, axis=0))
        return imgs

    # noinspection PyProtectedMember
    @tf.function
    def distributed_eval_step(dist_inputs):
        _arr = strategy.run(eval_step, args=(dist_inputs,))
        task_loss, lbl, logits, imgs = _arr

        if strategy.num_replicas_in_sync > 1:
            lbl = strategy.gather(lbl, 0)
            logits = strategy.gather(logits, 0)

        lbl = tf.cast(tf.squeeze(lbl), tf.float32)
        logits = tf.cast(tf.squeeze(logits), tf.float32)
        task_loss = strategy.reduce("SUM", task_loss, axis=None)

        for key_, val_ in zip(metrics._fields, metrics):
            if key_ == "total_loss__eval":
                val_.update_state(values=task_loss)
            if key_ == "mape__eval":
                val_.update_state(lbl, logits)
            if key_ == "channel_wise_loss__eval":
                mae_channel_wise = tf.math.reduce_mean(tf.math.abs(lbl - logits), axis=0)
                val_.update_state(values=mae_channel_wise)
            if key_ == "channel_wise_logits__eval":
                val_.update_state(values=tf.math.reduce_mean(logits, axis=0))
            if key_ == "channel_wise_labels__eval":
                val_.update_state(values=tf.math.reduce_mean(lbl, axis=0))
        return lbl, logits, imgs

    if args.fp16:
        # we use mixed FP16 precision
        # With this policy, layers use float16 computations and float32 variables.
        # Computations are done in float16 for performance, but variables must be kept in
        # float32 for numeric stability.
        policy = tfk.mixed_precision.Policy('mixed_float16')
        tfk.mixed_precision.set_global_policy(policy)

    # Set up Multi-GPU
    strategy = tf.distribute.MirroredStrategy(devices=args.gpus,
                                              cross_device_ops=tf.distribute.NcclAllReduce())

    # Get Data
    data = nn.get_data(args=args)
    data = strategy.experimental_distribute_dataset(data)

    args.per_device_batch_size = int(args.batch_size / strategy.num_replicas_in_sync)

    # Save the full config:
    with open(os.path.join(args.paths.log_dir, "config.txt"), 'w') as f_conf:
        json.dump(nn.without_keys(args.__dict__, ["logger"]), f_conf, indent=2)

    # Build metrics
    metrics = nn.get_metrics(args)
    # Get model
    with strategy.scope():
        base_model, train_info = nn.get_model(args)
        if args.augs_on_gpu:
            _augmentation = {"brightness": 0.25, "jitter": 0.2, "fill_value": 0,
                             "scale": (0.5, 1.0), "fill_scale": (0.05, 0.75),
                             "fill_ratio": (0.25, 2.5), "strength": 0.4,
                             "ratio": (1., 1.), "use_image_normalization": True}
            # if args.deep_root or "efficient" in args.model:  # Weaker Augmentation for Convolutional Stems/NN
            #     _augmentation = {"brightness": 0.2, "jitter": 0.1,
            #                      "scale": (0.5, 1.0), "fill_scale": (0.05, 0.15),
            #                      "fill_ratio": (0.5, 1.5)}
            # else:
            #     _augmentation = {"brightness": 0.5, "jitter": 0.2,
            #                      "scale": (0.2, 1.0), "fill_scale": (0.05, 0.3),
            #                      "fill_ratio": (0.25, 2.5)}

            img_shape = [args.image_size, args.image_size, args.image_channels]
            aug_model = nn.gpu_augmenter(in_shape=img_shape,
                                         **_augmentation)
            aug_model.build(input_shape=[None] + img_shape)
            aug_model.trainable = False

            model = tfk.Model(inputs=aug_model.input, outputs=base_model(aug_model.output),
                              name=args.model)

        # Setting up tensorboard
        summary_writer = tf.summary.create_file_writer(args.paths.log_dir)
        model.build(input_shape=[None] + img_shape)
        # Needed for correctly displaying the Output shapes in summary
        # see: https://stackoverflow.com/a/65956968/4550763
        model.call(tfkl.Input(shape=(args.image_size, args.image_size, args.image_channels)))

        # Print out model summary:
        if "efficient" in args.model:
            base_model.encoder.summary(print_fn=args.logger.info)
            base_model.reg_head.summary(print_fn=args.logger.info)
        else:
            base_model.summary(print_fn=args.logger.info)
        model.summary(print_fn=args.logger.info)

        # Define optimizer and learning rate schedule
        warm_up = int(5 * train_info.steps_per_epoch)  # Warmup for 5 epochs
        if args.lr == 0.:
            if "efficient" in args.model:
                init_lr = 0.016 * args.batch_size / 256  # See https://github.com/tensorflow/tpu/issues/509
            else:
                init_lr = 1e-4 * args.batch_size / 512  # See Table 9 in arXiv:2012.12877 [cs.CV]
                # init_lr = 5e-4 * args.batch_size / 512  # See Table 9 in arXiv:2012.12877 [cs.CV]
        else:
            init_lr = args.lr
        # Set the cosine learning rate scheduler, see arXiv:1812.01187 [cs.CV]
        learning_rate = nn.LinearCosineSchedule(initial_learning_rate=init_lr,
                                                warmup_steps=warm_up,
                                                total_steps=train_info.total_steps)

        # Optimizer
        if args.optimizer.lower() == "sgd":
            optimizer = tfk.optimizers.SGD(learning_rate=learning_rate,
                                           momentum=0.9,
                                           nesterov=False)
        elif args.optimizer.lower() == "adam":
            optimizer = tfk.optimizers.Adam(learning_rate=learning_rate,
                                            beta_1=0.9, beta_2=0.999)
        elif args.optimizer.lower() == "rms":
            optimizer = tfk.optimizers.RMSprop(learning_rate=learning_rate,
                                               momentum=0.9)
        elif args.optimizer.lower() == "adamw":
            # We use the same scheduler for the weight_decay as for the learning_rate
            optimizer = nn.AdamWeightDecay(learning_rate=learning_rate,
                                           weight_decay_rate=args.l2,
                                           beta_1=0.9, beta_2=0.999)
        else:
            raise LookupError("Not a valid optimizer")

        if args.fp16:
            # use a dynamic loss scale, see https://www.tensorflow.org/guide/mixed_precision
            optimizer = tfk.mixed_precision.LossScaleOptimizer(optimizer, dynamic=False if args.sam else True,
                                                               initial_scale=2 ** 4 if args.sam else None)

    # start training
    step = 0
    for epoch in range(args.epochs):
        if epoch % args.test_every_n_epoch == 0 and step > 0:
            it_test = tqdm(takewhile(lambda x: x[0] < args.eval_steps_per_epochs, enumerate(data)),
                           total=train_info.eval_steps_per_epoch)
            label_list = []
            logit_list = []
            for _, features in it_test:
                eval_returns = distributed_eval_step(features)
                label_list.extend(eval_returns[0].numpy())
                logit_list.extend(eval_returns[1].numpy())
                eval_images = eval_returns[2]
                args_tmp = (epoch + 1, args.epochs,
                            metrics.total_loss__eval.result().numpy(),
                            metrics.mape__eval.result().numpy()
                            )
                st = "Eval || Epoch: {}/{}, Task Loss: {:.5f}, MAPE: {:.2f}%".format(*args_tmp)
                it_test.set_postfix_str(st)

            args.logger.info(st)
            savez(os.path.join(args.paths.log_dir, "scratch_epoch-{}.npz".format(epoch)),
                  labels=label_list, logits=logit_list)

            if summary_writer is not None:
                with summary_writer.as_default():
                    if strategy.num_replicas_in_sync > 1:
                        eval_images = strategy.gather(eval_images, 0)

                    channel_wise_mae = metrics.channel_wise_loss__eval.result().numpy()
                    channel_wise_logits = metrics.channel_wise_logits__eval.result().numpy()
                    channel_wise_labels = metrics.channel_wise_labels__eval.result().numpy()
                    eval_task_heatmap = nn.get_task_heatmap(args, channel_wise_mae,
                                                            channel_wise_logits,
                                                            channel_wise_labels)

                    tf.summary.image("Test Images",
                                     nn.normalize_img(eval_images,
                                                      use_image_normalization=False)[0],
                                     step=epoch)
                    tf.summary.image("Task Heatmap Test", eval_task_heatmap, step=epoch)

                    tf.summary.histogram("Test/Images",
                                         tf.reshape(eval_images, [-1]),
                                         step=epoch)
                    for k, v in zip(metrics._fields, metrics):
                        if "eval" in k and "channel" not in k:
                            tf.summary.scalar(k.replace("__", "/"), v.result(), step=epoch)

        it_train = tqdm(takewhile(lambda x: x[0] < args.train_steps_per_epochs, enumerate(data)),
                        total=train_info.steps_per_epoch)
        for _, features in it_train:
            step += 1
            images = distributed_train_step(features)

            if step % args.log_steps == 0 or step == 1:
                with summary_writer.as_default():
                    if strategy.num_replicas_in_sync > 1:
                        images = strategy.gather(images, 0)

                    channel_wise_mae = metrics.channel_wise_loss__train.result().numpy()
                    channel_wise_logits = metrics.channel_wise_logits__train.result().numpy()
                    channel_wise_labels = metrics.channel_wise_labels__train.result().numpy()
                    train_task_heatmap = nn.get_task_heatmap(args, channel_wise_mae,
                                                             channel_wise_logits,
                                                             channel_wise_labels)

                    tf.summary.image("Training Images",
                                     nn.normalize_img(images, use_image_normalization=False)[0],
                                     step=step)
                    tf.summary.image("Task Heatmap Train", train_task_heatmap, step=step)

                    tf.summary.histogram("Train/Images",
                                         tf.reshape(images, [-1]),
                                         step=step)

                    tf.summary.scalar("learning_rate", learning_rate(float(step)), step=step)
                    for k, v in zip(metrics._fields, metrics):
                        if "train" in k and "channel" not in k:
                            tf.summary.scalar(k.replace("__", "/"), v.result(), step=step)

            args_tmp = (epoch + 1, args.epochs, step, train_info.total_steps,
                        metrics.total_loss__train.result().numpy(),
                        metrics.mape__train.result().numpy())
            st = "Train || Epoch: {}/{}, Step: {}/{}, Total Loss: {:.5f}, MAPE: {:.2f}%".format(*args_tmp)

            it_train.set_postfix_str(st)

        args.logger.info(st)

        for m in metrics:
            m.reset_states()

        if epoch % args.save_every_n_epoch == 0:
            model.save_weights(filepath=os.path.join(args.paths.weight_dir, "scratch_epoch-{}".format(epoch)),
                               save_format="tf")

    # save model after training is completed
    save_str = "scratch_model_final_after_epoch-{}".format(args.epochs)
    model.save_weights(filepath=os.path.join(args.paths.weight_dir, save_str), save_format="tf")


def main(args):
    # parse arguments
    if args is None:
        exit()

    # Build the model train it
    scratch(args)


if __name__ == "__main__":
    arguments = nn.get_args()
    arguments.pretrain = False
    arguments.paths = nn.set_path_env(arguments)

    # For logging:
    arguments.logger = tf.get_logger()
    arguments.logger.setLevel(logging.INFO)
    arguments.logger.propagate = False

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create file handler which logs even debug messages
    fh = logging.FileHandler(os.path.join(arguments.paths.log_dir, "train.log"))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    arguments.logger.addHandler(fh)
    if not arguments.gpus:
        arguments.logger.info("A supported GPU is necessary")
        # exit()
    else:
        physical_devices = tf.config.list_physical_devices('GPU')
        tf.config.threading.set_inter_op_parallelism_threads(2)
        tf.config.threading.set_intra_op_parallelism_threads(1)  # Avoid pool of Eigen threads
        # noinspection PyBroadException
        try:
            for i, g in enumerate(physical_devices):
                # if i < 4:  # GPU with index 4 is the student card on our machine
                tf.config.experimental.set_memory_growth(g, True)
        except:
            # Invalid device or cannot modify virtual devices once initialized.
            pass
        tf.config.set_soft_device_placement(True)
        logical_devices = tf.config.list_logical_devices('GPU')
        main(arguments)

        # close the handlers
        for handler in arguments.logger.handlers:
            handler.close()
            arguments.logger.removeFilter(handler)

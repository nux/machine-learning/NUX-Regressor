"""
This is performing the eval using multiple scalings and sensitivity holes
"""
import os
import nn
import json
import socket
import logging
from tqdm import tqdm
import tensorflow as tf
from numpy import savez
from itertools import takewhile
from collections import namedtuple
from types import SimpleNamespace

tfk = tf.keras
tfkl = tfk.layers


def eval_model(args):
    # noinspection PyProtectedMember
    @tf.function
    def distributed_eval_step(dist_inputs):

        @tf.function
        def log_fun(x):
            cond = tf.greater(x, 0.)
            return tf.where(cond, tf.math.log(x), x)

        funs = {"linear": tf.identity, "log": log_fun,
                "sigmoid": tf.math.sigmoid, "norm": tf.math.l2_normalize,
                "sqrt": tf.math.sqrt}

        results = {}
        for k, v in funs.items():
            _arr = strategy.run(eval_step, args=(dist_inputs, v,))
            task_loss, lbl, logits, imgs = _arr

            if strategy.num_replicas_in_sync > 1:
                lbl = strategy.gather(lbl, 0)
                logits = strategy.gather(logits, 0)

            lbl = tf.cast(tf.squeeze(lbl), tf.float32)
            logits = tf.cast(tf.squeeze(logits), tf.float32)
            task_loss = strategy.reduce("SUM", task_loss, axis=None)
            results.update({k: (lbl, logits, imgs)})

            for key_, val_ in zip(metrics._fields, metrics):
                if k == "linear":
                    if key_ == "total_loss__eval":
                        val_.update_state(values=task_loss)
                    if key_ == "channel_wise_loss__eval":
                        mae_channel_wise = tf.math.reduce_mean(tf.math.abs(lbl - logits), axis=0)
                        val_.update_state(values=mae_channel_wise)
                    if key_ == "channel_wise_logits__eval":
                        val_.update_state(values=tf.math.reduce_mean(logits, axis=0))
                    if key_ == "channel_wise_labels__eval":
                        val_.update_state(values=tf.math.reduce_mean(lbl, axis=0))
                else:
                    if key_ == "total_loss_{}__eval".format(k):
                        val_.update_state(values=task_loss)
                    if key_ == "channel_wise_loss_{}__eval".format(k):
                        mae_channel_wise = tf.math.reduce_mean(tf.math.abs(lbl - logits), axis=0)
                        val_.update_state(values=mae_channel_wise)
                    if key_ == "channel_wise_logits_{}__eval".format(k):
                        val_.update_state(values=tf.math.reduce_mean(logits, axis=0))
                    if key_ == "channel_wise_labels_{}__eval".format(k):
                        val_.update_state(values=tf.math.reduce_mean(lbl, axis=0))
        return results

    @tf.function
    def eval_step(dist_input, fun):
        image_batch, label_batch = dist_input[0], dist_input[1]

        logits, iimgs = model(fun(image_batch),
                              training=False)
        task_loss = nn.mae(labels=label_batch, preds=logits, args=args)

        ret_arr = [task_loss, label_batch, logits, iimgs]
        return ret_arr

    if args.fp16:
        # we use mixed FP16 precision
        # With this policy, layers use float16 computations and float32 variables.
        # Computations are done in float16 for performance, but variables must be kept in
        # float32 for numeric stability.
        policy = tfk.mixed_precision.Policy('mixed_float16')
        tfk.mixed_precision.set_global_policy(policy)

    # Set up Multi-GPU
    strategy = tf.distribute.MirroredStrategy(devices=args.gpus,
                                              cross_device_ops=tf.distribute.NcclAllReduce())

    # Get Data
    data = nn.get_data(args=args)
    data = strategy.experimental_distribute_dataset(data)

    args.per_device_batch_size = int(args.batch_size / strategy.num_replicas_in_sync)

    # Build metrics
    metrics = nn.get_metrics(args)
    # Get model
    with strategy.scope():
        base_model, train_info = nn.get_model(args)
        if args.augs_on_gpu:
            img_shape = [args.image_size, args.image_size, args.image_channels]
            aug_model = nn.gpu_augmenter(in_shape=img_shape)
            aug_model.build(input_shape=[None] + img_shape)
            aug_model.trainable = False

            model = tfk.Model(inputs=aug_model.input, outputs=base_model(aug_model.output),
                              name=args.model)
            model.trainable = False
            # Loads the weights
            checkpoint_path = tf.train.latest_checkpoint(args.paths.weight_dir)
            load_status = model.load_weights(checkpoint_path)
            # load_status = model.load_weights(checkpoint_path).expect_partial()
            load_status.assert_consumed().assert_nontrivial_match()

        # Setting up tensorboard
        summary_writer = tf.summary.create_file_writer(args.paths.log_dir)
        model.build(input_shape=[None] + img_shape)
        # Needed for correctly displaying the Output shapes in summary
        # see: https://stackoverflow.com/a/65956968/4550763
        model.call(tfkl.Input(shape=(args.image_size, args.image_size, args.image_channels)))

        # Print out model summary:
        base_model.summary(print_fn=args.logger.info)
        model.summary(print_fn=args.logger.info)

    it_test = tqdm(takewhile(lambda x: x[0] < args.eval_steps_per_epochs, enumerate(data)),
                   total=train_info.eval_steps_per_epoch)

    label_list_linear = []
    logit_list_linear = []
    label_list_log = []
    logit_list_log = []
    label_list_sigmoid = []
    logit_list_sigmoid = []
    label_list_norm = []
    logit_list_norm = []
    label_list_sqrt = []
    logit_list_sqrt = []
    for _, features in it_test:
        results_dict = distributed_eval_step(features)

        label_list_linear.extend(results_dict["linear"][0].numpy())
        logit_list_linear.extend(results_dict["linear"][1].numpy())
        eval_images_linear = results_dict["linear"][2]
        label_list_log.extend(results_dict["log"][0].numpy())
        logit_list_log.extend(results_dict["log"][1].numpy())
        eval_images_log = results_dict["log"][2]
        label_list_sigmoid.extend(results_dict["sigmoid"][0].numpy())
        logit_list_sigmoid.extend(results_dict["sigmoid"][1].numpy())
        eval_images_sigmoid = results_dict["sigmoid"][2]
        label_list_norm.extend(results_dict["norm"][0].numpy())
        logit_list_norm.extend(results_dict["norm"][1].numpy())
        eval_images_norm = results_dict["norm"][2]
        label_list_sqrt.extend(results_dict["sqrt"][0].numpy())
        logit_list_sqrt.extend(results_dict["sqrt"][1].numpy())
        eval_images_sqrt = results_dict["sqrt"][2]

        args_tmp = (
            metrics.total_loss__eval.result().numpy(),
            metrics.total_loss_log__eval.result().numpy(),
            metrics.total_loss_sigmoid__eval.result().numpy(),
            metrics.total_loss_norm__eval.result().numpy(),
            metrics.total_loss_sqrt__eval.result().numpy(),
        )
        st = "Eval Loss || Linear: {:.5f}, Log: {:.5f}, Sigmoid: " \
             "{:.5f}, Norm: {:.5f}, Sqrt: {:.5f}".format(*args_tmp)
        it_test.set_postfix_str(st)

    args.logger.info(st)
    savez(os.path.join(args.paths.log_dir, "full_eval_{}.npz".format(os.path.basename(checkpoint_path))),
          labels_linear=label_list_linear, logits_linear=logit_list_linear,
          labels_log=label_list_log, logits_log=logit_list_log,
          labels_sigmoid=label_list_sigmoid, logits_sigmoid=logit_list_sigmoid,
          labels_norm=label_list_norm, logits_norm=logit_list_norm,
          labels_sqrt=label_list_sqrt, logits_sqrt=logit_list_sqrt,
          )

    if summary_writer is not None:
        with summary_writer.as_default():
            if strategy.num_replicas_in_sync > 1:
                eval_images_linear = strategy.gather(eval_images_linear, 0)
                eval_images_log = strategy.gather(eval_images_log, 0)
                eval_images_sigmoid = strategy.gather(eval_images_sigmoid, 0)
                eval_images_norm = strategy.gather(eval_images_norm, 0)
                eval_images_sqrt = strategy.gather(eval_images_sqrt, 0)

            base_str = os.path.basename(checkpoint_path)
            epoch = int(base_str[-base_str[::-1].find("-"):]) + 1

            channel_wise_mae_linear = metrics.channel_wise_loss__eval.result().numpy()
            channel_wise_logits_linear = metrics.channel_wise_logits__eval.result().numpy()
            channel_wise_labels_linear = metrics.channel_wise_labels__eval.result().numpy()
            eval_task_heatmap_linear = nn.get_task_heatmap(args, channel_wise_mae_linear,
                                                           channel_wise_logits_linear,
                                                           channel_wise_labels_linear)

            tf.summary.image("Test Images (linear)",
                             nn.normalize_img(eval_images_linear,
                                              use_image_normalization=False)[0],
                             step=epoch)
            tf.summary.image("Task Heatmap Test (linear)", eval_task_heatmap_linear, step=epoch)

            tf.summary.histogram("Test/Images (linear)",
                                 tf.reshape(eval_images_linear, [-1]),
                                 step=epoch)

            channel_wise_mae_log = metrics.channel_wise_loss_log__eval.result().numpy()
            channel_wise_logits_log = metrics.channel_wise_logits_log__eval.result().numpy()
            channel_wise_labels_log = metrics.channel_wise_labels_log__eval.result().numpy()
            eval_task_heatmap_log = nn.get_task_heatmap(args, channel_wise_mae_log,
                                                        channel_wise_logits_log,
                                                        channel_wise_labels_log)

            tf.summary.image("Test Images (log)",
                             nn.normalize_img(eval_images_log,
                                              use_image_normalization=False)[0],
                             step=epoch)
            tf.summary.image("Task Heatmap Test (log)", eval_task_heatmap_log, step=epoch)

            tf.summary.histogram("Test/Images (log)",
                                 tf.reshape(eval_images_log, [-1]),
                                 step=epoch)

            channel_wise_mae_sigmoid = metrics.channel_wise_loss_sigmoid__eval.result().numpy()
            channel_wise_logits_sigmoid = metrics.channel_wise_logits_sigmoid__eval.result().numpy()
            channel_wise_labels_sigmoid = metrics.channel_wise_labels_sigmoid__eval.result().numpy()
            eval_task_heatmap_sigmoid = nn.get_task_heatmap(args, channel_wise_mae_sigmoid,
                                                            channel_wise_logits_sigmoid,
                                                            channel_wise_labels_sigmoid)

            tf.summary.image("Test Images (sigmoid)",
                             nn.normalize_img(eval_images_sigmoid,
                                              use_image_normalization=False)[0],
                             step=epoch)
            tf.summary.image("Task Heatmap Test (sigmoid)", eval_task_heatmap_sigmoid, step=epoch)

            tf.summary.histogram("Test/Images (sigmoid)",
                                 tf.reshape(eval_images_linear, [-1]),
                                 step=epoch)

            channel_wise_mae_norm = metrics.channel_wise_loss_norm__eval.result().numpy()
            channel_wise_logits_norm = metrics.channel_wise_logits_norm__eval.result().numpy()
            channel_wise_labels_norm = metrics.channel_wise_labels_norm__eval.result().numpy()
            eval_task_heatmap_norm = nn.get_task_heatmap(args, channel_wise_mae_norm,
                                                         channel_wise_logits_norm,
                                                         channel_wise_labels_norm)

            tf.summary.image("Test Images (norm)",
                             nn.normalize_img(eval_images_norm,
                                              use_image_normalization=False)[0],
                             step=epoch)
            tf.summary.image("Task Heatmap Test (norm)", eval_task_heatmap_norm, step=epoch)

            tf.summary.histogram("Test/Images (norm)",
                                 tf.reshape(eval_images_norm, [-1]),
                                 step=epoch)

            channel_wise_mae_sqrt = metrics.channel_wise_loss_sqrt__eval.result().numpy()
            channel_wise_logits_sqrt = metrics.channel_wise_logits_sqrt__eval.result().numpy()
            channel_wise_labels_sqrt = metrics.channel_wise_labels_sqrt__eval.result().numpy()
            eval_task_heatmap_sqrt = nn.get_task_heatmap(args, channel_wise_mae_sqrt,
                                                         channel_wise_logits_sqrt,
                                                         channel_wise_labels_sqrt)

            tf.summary.image("Test Images (sqrt)",
                             nn.normalize_img(eval_images_sqrt,
                                              use_image_normalization=False)[0],
                             step=epoch)
            tf.summary.image("Task Heatmap Test (sqrt)", eval_task_heatmap_sqrt, step=epoch)

            tf.summary.histogram("Test/Images (sqrt)",
                                 tf.reshape(eval_images_sqrt, [-1]),
                                 step=epoch)

            for k, v in zip(metrics._fields, metrics):
                if "eval" in k and "channel" not in k:
                    tf.summary.scalar(k.replace("__", "/"), v.result(), step=epoch)


def main(args):
    # parse arguments
    if args is None:
        exit()

    # Build the model, load the weights and eval it
    eval_model(args)


if __name__ == "__main__":
    arguments = nn.get_args()
    gpus = arguments.gpus
    scat_gpus = arguments.scatman_gpu
    eval_steps_per_epochs = arguments.eval_steps_per_epochs

    # Load the config of the pretrained model
    conf_path = os.path.join(arguments.base_model_dir, "logs", "config.txt")
    if not os.path.isfile(conf_path):
        raise FileNotFoundError("Config file not found at: {}".format(conf_path))

    with open(conf_path, 'r') as f:
        arguments = json.load(f)

    arguments = SimpleNamespace(**arguments)

    arguments.pretrain = False
    arguments.full_eval = True
    arguments.gpus = gpus
    arguments.scatman_gpu = scat_gpus
    arguments.eval_steps_per_epochs = eval_steps_per_epochs
    path_tuple = namedtuple("path_tuple", "work_dir weight_dir log_dir base_dir")
    arguments.paths = path_tuple(work_dir=arguments.paths[0], weight_dir=arguments.paths[1],
                                 log_dir=arguments.paths[2], base_dir=arguments.paths[3])
    # For logging:
    arguments.logger = tf.get_logger()
    arguments.logger.setLevel(logging.INFO)
    arguments.logger.propagate = False
    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # create file handler which logs even debug messages
    fh = logging.FileHandler(os.path.join(arguments.paths.log_dir, "train.log"))
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(formatter)

    arguments.logger.addHandler(fh)
    if not arguments.gpus:
        arguments.logger.info("A supported GPU is necessary")
        # exit()
    else:
        physical_devices = tf.config.list_physical_devices('GPU')
        tf.config.threading.set_inter_op_parallelism_threads(2)
        tf.config.threading.set_intra_op_parallelism_threads(1)  # Avoid pool of Eigen threads
        # noinspection PyBroadException
        try:
            for i, g in enumerate(physical_devices):
                # if i < 4:  # GPU with index 4 is the student card on our machine
                tf.config.experimental.set_memory_growth(g, True)
        except:
            # Invalid device or cannot modify virtual devices once initialized.
            pass
        tf.config.set_soft_device_placement(True)
        logical_devices = tf.config.list_logical_devices('GPU')
        main(arguments)

        # close the handlers
        for handler in arguments.logger.handlers:
            handler.close()
            arguments.logger.removeFilter(handler)
